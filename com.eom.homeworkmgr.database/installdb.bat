@echo off
SET DERBY_HOME=C:\Projekte\Hausaufgabenmanager3000\hausaufgabenmanager-3000\com.eom.homeworkmgr.database\db-derby-10.9.1.0-bin
echo -- Installing DB
IF EXIST "..\com.eom.homeworkmgr.data\homeworkmgr.db" (
    rd ..\com.eom.homeworkmgr.data\homeworkmgr.db /s /q
    echo --- Existing DB deleted.
)

cd sql
CALL "../db-derby-10.9.1.0-bin/bin/ij.bat" "create_db.sql"
cd ..
echo --- DB Installed

pause
