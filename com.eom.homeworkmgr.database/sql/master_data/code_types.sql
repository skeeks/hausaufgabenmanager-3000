
-- Create Subject Codes ---------------------------------------------------
INSERT INTO EOM_UC (UC_UID, CODE_TYPE_UID, EXT_KEY) VALUES (1003, 1001, '#af643a');
INSERT INTO EOM_UCL 
	(UC_UID, LANG_UID, CODE_NAME) 
VALUES 
	(1003, 101, 'History'), (1003, 100, 'Geschichte'), (1003, 102, 'Histoire');
---------------------------------------------------------------------------
INSERT INTO EOM_UC (UC_UID, CODE_TYPE_UID, EXT_KEY) VALUES (1004, 1001, '#2c8344');
INSERT INTO EOM_UCL 
	(UC_UID, LANG_UID, CODE_NAME) 
VALUES 
	(1004, 101, 'Physics'), (1004, 100, 'Physik'), (1004, 102, 'Physique');
---------------------------------------------------------------------------
INSERT INTO EOM_UC (UC_UID, CODE_TYPE_UID, EXT_KEY) VALUES (1025, 1001, '#c7eccd');
INSERT INTO EOM_UCL 
	(UC_UID, LANG_UID, CODE_NAME) 
VALUES 
	(1025, 101, 'Mathematics'), (1025, 100, 'Mathematik'), (1025, 102, 'Mathematique');
---------------------------------------------------------------------------
INSERT INTO EOM_UC (UC_UID, CODE_TYPE_UID, EXT_KEY) VALUES (1026, 1001, '#0d2624');
INSERT INTO EOM_UCL 
	(UC_UID, LANG_UID, CODE_NAME) 
VALUES 
	(1026, 101, 'German'), (1026, 100, 'Deutsch'), (1026, 102, 'Allemand');
---------------------------------------------------------------------------
INSERT INTO EOM_UC (UC_UID, CODE_TYPE_UID, EXT_KEY) VALUES (1027, 1001, '#9fcfdf');
INSERT INTO EOM_UCL 
	(UC_UID, LANG_UID, CODE_NAME) 
VALUES 
	(1027, 101, 'English'), (1027, 100, 'Englisch'), (1027, 102, 'Anglaise');
---------------------------------------------------------------------------
INSERT INTO EOM_UC (UC_UID, CODE_TYPE_UID, EXT_KEY) VALUES (1028, 1001, '#5c70c9');
INSERT INTO EOM_UCL 
	(UC_UID, LANG_UID, CODE_NAME) 
VALUES 
	(1028, 101, 'Informatics'), (1028, 100, 'Informatik'), (1028, 102, 'Informatique');
---------------------------------------------------------------------------
