
-- CALL OTHER 

RUN 'tables/create_eom_seq.sql';
RUN 'tables/create_eom_lesson.sql';
RUN 'tables/create_eom_note.sql';
RUN 'tables/create_eom_task.sql';
RUN 'tables/create_eom_uc.sql';
RUN 'tables/create_eom_ucl.sql';

-- CALL MASTER DATA SETUP
RUN 'master_data/code_types.sql';
RUN 'master_data/lesson.sql';
RUN 'master_data/notes.sql';
RUN 'master_data/tasks.sql';
RUN 'master_data/exams.sql';
