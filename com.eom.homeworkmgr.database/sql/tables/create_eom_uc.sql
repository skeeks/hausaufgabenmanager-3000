CREATE TABLE EOM_UC
(
UC_UID BIGINT NOT NULL,
CODE_TYPE_UID BIGINT NOT NULL,
EXT_KEY VARCHAR(200),
PARENT_KEY BIGINT,
PRIMARY KEY (UC_UID)
)