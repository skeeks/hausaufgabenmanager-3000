CREATE TABLE EOM_TASK
(
TASK_NR BIGINT NOT NULL,
TASK_DATE DATE NOT NULL,
SUBJECT_UID BIGINT NOT NULL,
DESCRIPTION VARCHAR(2000) NOT NULL,
IS_EXAM BOOLEAN,
IS_DONE BOOLEAN,
PRIMARY KEY (TASK_NR)
)