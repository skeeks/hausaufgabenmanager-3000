CREATE TABLE EOM_LESSON
(
	LESSON_NR BIGINT NOT NULL,
	WEEKDAY_UID BIGINT NOT NULL,
	START_TIME TIME NOT NULL,
	END_TIME TIME NOT NULL,
	SUBJECT_UID BIGINT,
	TEACHER VARCHAR(200) NOT NULL DEFAULT '',
	PRIMARY KEY (LESSON_NR)
)