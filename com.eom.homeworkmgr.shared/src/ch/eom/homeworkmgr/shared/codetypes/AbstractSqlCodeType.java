package ch.eom.homeworkmgr.shared.codetypes;

import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeTypeWithGeneric;
import org.eclipse.scout.rt.shared.services.common.code.ICode;
import org.eclipse.scout.rt.shared.services.common.code.ICodeRow;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.shared.services.ISqlCodeTypeLoaderService;

public abstract class AbstractSqlCodeType extends AbstractCodeTypeWithGeneric<Long, Long, ICode<Long>> {

  private static final long serialVersionUID = 1L;

  @Override
  protected List<? extends ICodeRow<Long>> execLoadCodes(Class<? extends ICodeRow<Long>> codeRowType) throws ProcessingException {
    return SERVICES.getService(ISqlCodeTypeLoaderService.class).loadCodes(this.getId());
  }

}
