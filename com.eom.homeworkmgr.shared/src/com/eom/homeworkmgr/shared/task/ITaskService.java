/**
 *
 */
package com.eom.homeworkmgr.shared.task;

import java.util.Collection;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

/**
 * @author sei
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface ITaskService extends IService {

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  TaskFormData create(TaskFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  TaskFormData load(TaskFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  TaskFormData prepareCreate(TaskFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  TaskFormData store(TaskFormData formData) throws ProcessingException;

  /**
   * @return
   * @throws ProcessingException
   */
  TaskTablePageData getTaskTableData(TaskSearchFormData filter) throws ProcessingException;

  /**
   * @param taskNr
   * @param done
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  void updateDone(Long taskNr, Boolean done) throws ProcessingException;

  /**
   * @param arrayList
   */
  int delete(Collection<Long> arrayList) throws ProcessingException;
}
