/**
 *
 */
package com.eom.homeworkmgr.shared.services;

import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.services.common.code.ICodeRow;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

/**
 * @author sixkn_000
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface ISqlCodeTypeLoaderService extends IService {

  /**
   * @param codeTypeUid
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  List<? extends ICodeRow<Long>> loadCodes(Long codeTypeUid) throws ProcessingException;
}
