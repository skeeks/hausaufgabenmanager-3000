/**
 *
 */
package com.eom.homeworkmgr.shared.services;

import java.util.Locale;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

/**
 * Mainly used for init purposes.
 *
 * @author ske
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface IEomService extends IService {

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  void initDbConnection() throws ProcessingException;

  /**
   * Sets the language of the program.
   *
   * @throws ProcessingException
   */
  public Locale initLocale() throws ProcessingException;

  /**
   * @param languageUid
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  void setLanguage(Long languageUid) throws ProcessingException;

}
