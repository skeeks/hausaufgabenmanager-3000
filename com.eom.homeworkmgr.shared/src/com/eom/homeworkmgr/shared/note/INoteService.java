/**
 *
 */
package com.eom.homeworkmgr.shared.note;

import java.util.Collection;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

/**
 * @author sei
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface INoteService extends IService {

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  NoteFormData create(NoteFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  NoteFormData load(NoteFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  NoteFormData prepareCreate(NoteFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  NoteFormData store(NoteFormData formData) throws ProcessingException;

  /**
   * @return
   * @throws ProcessingException
   */
  NoteTablePageData getNoteTableData(NoteSearchFormData formData) throws ProcessingException;

  /**
   * @param arrayList
   */
  int delete(Collection<Long> arrayList) throws ProcessingException;
}
