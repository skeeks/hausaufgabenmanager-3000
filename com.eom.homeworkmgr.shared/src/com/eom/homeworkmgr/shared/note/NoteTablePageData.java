/**
 * 
 */
package com.eom.homeworkmgr.shared.note;

import java.util.Date;

import javax.annotation.Generated;

import org.eclipse.scout.rt.shared.data.basic.table.AbstractTableRowData;
import org.eclipse.scout.rt.shared.data.page.AbstractTablePageData;

/**
 * <b>NOTE:</b><br>
 * This class is auto generated by the Scout SDK. No manual modifications recommended.
 * 
 * @generated
 */
@Generated(value = "org.eclipse.scout.sdk.workspace.dto.pagedata.PageDataDtoUpdateOperation", comments = "This class is auto generated by the Scout SDK. No manual modifications recommended.")
public class NoteTablePageData extends AbstractTablePageData {

  private static final long serialVersionUID = 1L;

  public NoteTablePageData() {
  }

  @Override
  public NoteTableRowData addRow() {
    return (NoteTableRowData) super.addRow();
  }

  @Override
  public NoteTableRowData addRow(int rowState) {
    return (NoteTableRowData) super.addRow(rowState);
  }

  @Override
  public NoteTableRowData createRow() {
    return new NoteTableRowData();
  }

  @Override
  public Class<? extends AbstractTableRowData> getRowType() {
    return NoteTableRowData.class;
  }

  @Override
  public NoteTableRowData[] getRows() {
    return (NoteTableRowData[]) super.getRows();
  }

  @Override
  public NoteTableRowData rowAt(int index) {
    return (NoteTableRowData) super.rowAt(index);
  }

  public void setRows(NoteTableRowData[] rows) {
    super.setRows(rows);
  }

  public static class NoteTableRowData extends AbstractTableRowData {

    private static final long serialVersionUID = 1L;
    public static final String noteNr = "noteNr";
    public static final String date = "date";
    public static final String subject = "subject";
    public static final String description = "description";
    private Long m_noteNr;
    private Date m_date;
    private Long m_subject;
    private String m_description;

    public NoteTableRowData() {
    }

    /**
     * @return the NoteNr
     */
    public Long getNoteNr() {
      return m_noteNr;
    }

    /**
     * @param noteNr
     *          the NoteNr to set
     */
    public void setNoteNr(Long noteNr) {
      m_noteNr = noteNr;
    }

    /**
     * @return the Date
     */
    public Date getDate() {
      return m_date;
    }

    /**
     * @param date
     *          the Date to set
     */
    public void setDate(Date date) {
      m_date = date;
    }

    /**
     * @return the Subject
     */
    public Long getSubject() {
      return m_subject;
    }

    /**
     * @param subject
     *          the Subject to set
     */
    public void setSubject(Long subject) {
      m_subject = subject;
    }

    /**
     * @return the Description
     */
    public String getDescription() {
      return m_description;
    }

    /**
     * @param description
     *          the Description to set
     */
    public void setDescription(String description) {
      m_description = description;
    }
  }
}
