/**
 * 
 */
package com.eom.homeworkmgr.shared.subject;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.TEXTS;

import ch.eom.homeworkmgr.shared.codetypes.AbstractSqlCodeType;

/**
 * @author sei
 */
public class SubjectCodeType extends AbstractSqlCodeType {

  private static final long serialVersionUID = 1L;
  /**
 * 
 */
  public static final Long ID = 1001L;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public SubjectCodeType() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredText() {
    return TEXTS.get("Subject0");
  }

  @Override
  public Long getId() {
    return ID;
  }
}
