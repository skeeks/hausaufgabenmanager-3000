/**
 *
 */
package com.eom.homeworkmgr.shared.weekday;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

/**
 * @author ske
 */
public class WeekdayCodeType extends AbstractCodeType<Long, Long> {

  private static final long serialVersionUID = 1L;
  /**
   *
   */
  public static final Long ID = 1009L;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public WeekdayCodeType() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredText() {
    return TEXTS.get("Weekday");
  }

  @Override
  public Long getId() {
    return ID;
  }

  @Order(10.0)
  public static class MondayCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;

    public static final Long ID = 1010L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Monday");
    }

    @Override
    public Long getId() {
      return ID;
    }
  }

  @Order(20.0)
  public static class TuesdayCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;

    public static final Long ID = 1011L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Tuesday");
    }

    @Override
    public Long getId() {
      return ID;
    }
  }

  @Order(30.0)
  public static class WednesdayCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;

    public static final Long ID = 1012L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Wednesday");
    }

    @Override
    public Long getId() {
      return ID;
    }
  }

  @Order(40.0)
  public static class ThursdayCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;

    public static final Long ID = 1013L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Thursday");
    }

    @Override
    public Long getId() {
      return ID;
    }
  }

  @Order(50.0)
  public static class FridayCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;

    public static final Long ID = 1014L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Friday");
    }

    @Override
    public Long getId() {
      return ID;
    }
  }

  @Order(60.0)
  public static class SaturdayCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;
    /**
     *
     */
    public static final Long ID = 1015L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Saturday");
    }

    @Override
    public Long getId() {
      return ID;
    }
  }

  @Order(70.0)
  public static class SundayCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;
    /**
     *
     */
    public static final Long ID = 1016L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Sunday");
    }

    @Override
    public Long getId() {
      return ID;
    }
  }
}
