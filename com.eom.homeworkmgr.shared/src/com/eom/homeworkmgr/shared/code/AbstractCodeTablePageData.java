/**
 *
 */
package com.eom.homeworkmgr.shared.code;

import org.eclipse.scout.rt.shared.data.basic.table.AbstractTableRowData;
import org.eclipse.scout.rt.shared.data.page.AbstractTablePageData;

public class AbstractCodeTablePageData extends AbstractTablePageData {

  private static final long serialVersionUID = 1L;

  public AbstractCodeTablePageData() {
  }

  @Override
  public AbstractCodeTableRowData addRow() {
    return (AbstractCodeTableRowData) super.addRow();
  }

  @Override
  public AbstractCodeTableRowData addRow(int rowState) {
    return (AbstractCodeTableRowData) super.addRow(rowState);
  }

  @Override
  public AbstractCodeTableRowData createRow() {
    return new AbstractCodeTableRowData();
  }

  @Override
  public Class<? extends AbstractTableRowData> getRowType() {
    return AbstractCodeTableRowData.class;
  }

  @Override
  public AbstractCodeTableRowData[] getRows() {
    return (AbstractCodeTableRowData[]) super.getRows();
  }

  @Override
  public AbstractCodeTableRowData rowAt(int index) {
    return (AbstractCodeTableRowData) super.rowAt(index);
  }

  public void setRows(AbstractCodeTableRowData[] rows) {
    super.setRows(rows);
  }

  public static class AbstractCodeTableRowData extends AbstractTableRowData {

    private static final long serialVersionUID = 1L;
    public static final String codeNr = "codeNr";
    public static final String extKeyColor = "extKeyColor";
    public static final String german = "german";
    public static final String english = "english";
    public static final String french = "french";
    private Long m_codeNr;
    private String m_extKeyColor;
    private String m_german;
    private String m_english;
    private String m_french;

    public AbstractCodeTableRowData() {
    }

    /**
     * @return the CodeNr
     */
    public Long getCodeNr() {
      return m_codeNr;
    }

    /**
     * @param codeNr
     *          the CodeNr to set
     */
    public void setCodeNr(Long codeNr) {
      m_codeNr = codeNr;
    }

    /**
     * @return the Value
     */
    public String getExtKeyColor() {
      return m_extKeyColor;
    }

    /**
     * @param value
     *          the Value to set
     */
    public void setExtKeyColor(String value) {
      m_extKeyColor = value;
    }

    /**
     * @return the German
     */
    public String getGerman() {
      return m_german;
    }

    /**
     * @param german
     *          the German to set
     */
    public void setGerman(String german) {
      m_german = german;
    }

    /**
     * @return the English
     */
    public String getEnglish() {
      return m_english;
    }

    /**
     * @param english
     *          the English to set
     */
    public void setEnglish(String english) {
      m_english = english;
    }

    /**
     * @return the French
     */
    public String getFrench() {
      return m_french;
    }

    /**
     * @param french
     *          the French to set
     */
    public void setFrench(String french) {
      m_french = french;
    }
  }
}
