/**
 *
 */
package com.eom.homeworkmgr.shared.code;

import java.util.Collection;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

/**
 * @author sei
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface ICodePageService extends IService {

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodeFormData load(CodeFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodeFormData prepareCreate(CodeFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodeFormData store(CodeFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws ProcessingException
   */
  AbstractCodeTablePageData getCodeTableData(Long codeTypeUid) throws ProcessingException;

  /**
   * @param formData
   * @param codeType
   * @return
   * @throws ProcessingException
   */
  CodeFormData create(CodeFormData formData) throws ProcessingException;

  /**
   * @param codeNrs
   */
  int delete(Collection<Long> codeNrs) throws ProcessingException;
}
