/**
 *
 */
package com.eom.homeworkmgr.shared.timetable;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

/**
 * @author sei
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface ILessonService extends IService {

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  LessonFormData create(LessonFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  LessonFormData load(LessonFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  LessonFormData prepareCreate(LessonFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  LessonFormData store(LessonFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  void delete(Long timetableNr) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  TimetableFieldData loadTimetable(TimetableFieldData formData) throws ProcessingException;

  /**
   * @param formData
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  void checkForLessonCollision(LessonFormData formData) throws ProcessingException;
}
