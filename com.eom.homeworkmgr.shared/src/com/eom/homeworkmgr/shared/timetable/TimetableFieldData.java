/**
 *
 */
package com.eom.homeworkmgr.shared.timetable;

import java.util.Date;

import org.eclipse.scout.rt.shared.data.form.fields.tablefield.AbstractTableFieldData;

public class TimetableFieldData extends AbstractTableFieldData {

  private static final long serialVersionUID = 1L;
  public static final int LESSON_NR_COLUMN_ID = 0;
  public static final int SUBJECT_COLUMN_ID = 1;
  public static final int WEEKDAY_COLUMN_ID = 2;
  public static final int START_TIME_COLUMN_ID = 3;
  public static final int END_TIME_COLUMN_ID = 4;
  public static final int TEACHER_COLUMN_ID = 5;

  public TimetableFieldData() {
  }

  public Date getEndTime(int row) {
    return (Date) getValueInternal(row, END_TIME_COLUMN_ID);
  }

  public void setEndTime(int row, Date endTime) {
    setValueInternal(row, END_TIME_COLUMN_ID, endTime);
  }

  public Date getStartTime(int row) {
    return (Date) getValueInternal(row, START_TIME_COLUMN_ID);
  }

  public void setStartTime(int row, Date startTime) {
    setValueInternal(row, START_TIME_COLUMN_ID, startTime);
  }

  public Long getSubject(int row) {
    return (Long) getValueInternal(row, SUBJECT_COLUMN_ID);
  }

  public void setSubject(int row, Long subject) {
    setValueInternal(row, SUBJECT_COLUMN_ID, subject);
  }

  public Long getLessonNr(int row) {
    return (Long) getValueInternal(row, LESSON_NR_COLUMN_ID);
  }

  public void setLessonNr(int row, Long lessonNr) {
    setValueInternal(row, LESSON_NR_COLUMN_ID, lessonNr);
  }

  public Long getWeekday(int row) {
    return (Long) getValueInternal(row, WEEKDAY_COLUMN_ID);
  }

  public void setWeekday(int row, Long weekday) {
    setValueInternal(row, WEEKDAY_COLUMN_ID, weekday);
  }

  public String getTeacher(int row) {
    return (String) getValueInternal(row, TEACHER_COLUMN_ID);
  }

  public void setTeacher(int row, String teacher) {
    setValueInternal(row, TEACHER_COLUMN_ID, teacher);
  }

  @Override
  public int getColumnCount() {
    return 6;
  }

  @Override
  public Object getValueAt(int row, int column) {
    switch (column) {
      case LESSON_NR_COLUMN_ID:
        return getLessonNr(row);
      case SUBJECT_COLUMN_ID:
        return getSubject(row);
      case WEEKDAY_COLUMN_ID:
        return getWeekday(row);
      case START_TIME_COLUMN_ID:
        return getStartTime(row);
      case END_TIME_COLUMN_ID:
        return getEndTime(row);
      case TEACHER_COLUMN_ID:
        return getTeacher(row);
      default:
        return null;
    }
  }

  @Override
  public void setValueAt(int row, int column, Object value) {
    switch (column) {
      case LESSON_NR_COLUMN_ID:
        setLessonNr(row, (Long) value);
        break;
      case SUBJECT_COLUMN_ID:
        setSubject(row, (Long) value);
        break;
      case WEEKDAY_COLUMN_ID:
        setWeekday(row, (Long) value);
        break;
      case START_TIME_COLUMN_ID:
        setStartTime(row, (Date) value);
        break;
      case END_TIME_COLUMN_ID:
        setEndTime(row, (Date) value);
        break;
      case TEACHER_COLUMN_ID:
        setTeacher(row, (String) value);
        break;
    }
  }
}
