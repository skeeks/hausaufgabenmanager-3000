/**
 *
 */
package com.eom.homeworkmgr.shared.lang;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

/**
 * @author sei
 */
public class LanguageCodeType extends AbstractCodeType<Long, Long> {

  private static final long serialVersionUID = 1L;
  /**
   *
   */
  public static final Long ID = 1002L;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public LanguageCodeType() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredText() {
    return TEXTS.get("Language");
  }

  @Override
  public Long getId() {
    return ID;
  }

  @Order(10.0)
  public static class GermanCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;
    /**
     *
     */
    public static final Long ID = 100L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("German");
    }

    @Override
    public String getConfiguredExtKey() {
      return "de_CH";
    }

    @Override
    public Long getId() {
      return ID;
    }

  }

  @Order(20.0)
  public static class EnglishCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;
    /**
     *
     */
    public static final Long ID = 101L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("English");
    }

    @Override
    public String getConfiguredExtKey() {
      return "en";
    }

    @Override
    public Long getId() {
      return ID;
    }
  }

  @Order(30.0)
  public static class FrenchCode extends AbstractCode<Long> {

    private static final long serialVersionUID = 1L;
    /**
     *
     */
    public static final Long ID = 102L;

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("French");
    }

    @Override
    public String getConfiguredExtKey() {
      return "fr";
    }

    @Override
    public Long getId() {
      return ID;
    }
  }
}
