/**
 *
 */
package com.eom.homeworkmgr.server.code;

import java.util.Collection;

import org.eclipse.scout.commons.CollectionUtility;
import org.eclipse.scout.commons.annotations.SqlBindingIgnoreValidation;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.holders.NVPair;
import org.eclipse.scout.rt.server.services.common.jdbc.SQL;
import org.eclipse.scout.rt.shared.services.common.code.CODES;
import org.eclipse.scout.service.AbstractService;

import com.eom.homeworkmgr.shared.code.AbstractCodeTablePageData;
import com.eom.homeworkmgr.shared.code.CodeFormData;
import com.eom.homeworkmgr.shared.code.ICodePageService;
import com.eom.homeworkmgr.shared.lang.LanguageCodeType;

/**
 * @author sei
 */
public class CodePageService extends AbstractService implements ICodePageService {

  @Override
  @SqlBindingIgnoreValidation({"german", "english", "codeNr", "value", "french"})
  public AbstractCodeTablePageData getCodeTableData(Long codeTypeUid) throws ProcessingException {

    AbstractCodeTablePageData pageData = new AbstractCodeTablePageData();
    SQL.selectInto(
        "SELECT     uc.UC_UID, " +
            "       uc.EXT_KEY, " +
            "       (SELECT CODE_NAME FROM EOM_UCL uclde WHERE LANG_UID = :deCode AND UC_UID = uc.UC_UID), " +
            "       (SELECT CODE_NAME FROM EOM_UCL uclen WHERE LANG_UID = :enCode AND UC_UID = uc.UC_UID), " +
            "       (SELECT CODE_NAME FROM EOM_UCL uclfr WHERE LANG_UID = :frCode AND UC_UID = uc.UC_UID) " +
            " FROM  EOM_UC uc " +
            " WHERE 1=1 " +
            " AND uc.CODE_TYPE_UID = :codeType " +
            " INTO :codeNr, :extKeyColor, :german, :english, :french ",
        pageData,
        new NVPair("codeType", codeTypeUid),
        new NVPair("deCode", LanguageCodeType.GermanCode.ID),
        new NVPair("enCode", LanguageCodeType.EnglishCode.ID),
        new NVPair("frCode", LanguageCodeType.FrenchCode.ID));

    return pageData;
  }

  @Override
  public CodeFormData create(CodeFormData formData) throws ProcessingException {
    long nextPrimaryKey = SQL.getSequenceNextval("EOM_SEQ");
    SQL.insert("" +
        "INSERT INTO EOM_UC (UC_UID, CODE_TYPE_UID, EXT_KEY) " +
        "VALUES (:primaryKey, :codeTypeUid, :extKeyColor)"
        , formData, new NVPair("primaryKey", nextPrimaryKey),
        new NVPair("codeTypeUid", formData.getCodeTypeUid()));

    SQL.insert("" +
        "INSERT INTO EOM_UCL (UC_UID, LANG_UID, CODE_NAME) " +
        "VALUES (:primaryKey, :deCode, :german)"
        , formData, new NVPair("primaryKey", nextPrimaryKey),
        new NVPair("deCode", LanguageCodeType.GermanCode.ID));

    if (formData.getEnglish().getValue() == null) {
      formData.getEnglish().setValue("");
    }
    SQL.insert("" +
        "INSERT INTO EOM_UCL (UC_UID, LANG_UID, CODE_NAME) " +
        "VALUES (:primaryKey, :enCode, :english)"
        , formData, new NVPair("primaryKey", nextPrimaryKey),
        new NVPair("enCode", LanguageCodeType.EnglishCode.ID));

    if (formData.getFrench().getValue() == null) {
      formData.getFrench().setValue("");
    }
    SQL.insert("" +
        "INSERT INTO EOM_UCL (UC_UID, LANG_UID, CODE_NAME) " +
        "VALUES (:primaryKey, :frCode, :french)"
        , formData, new NVPair("primaryKey", nextPrimaryKey),
        new NVPair("frCode", LanguageCodeType.FrenchCode.ID));

    CODES.reloadCodeType(formData.getCodeTypeClass());

    return formData;
  }

  @Override
  public CodeFormData load(CodeFormData formData) throws ProcessingException {
    SQL.selectInto("" +
        "SELECT uc.UC_UID, " +
        "       uc.EXT_KEY, " +
        "       (SELECT CODE_NAME FROM EOM_UCL uclde WHERE LANG_UID = :deCode AND UC_UID = uc.UC_UID), " +
        "       (SELECT CODE_NAME FROM EOM_UCL uclen WHERE LANG_UID = :enCode AND UC_UID = uc.UC_UID), " +
        "       (SELECT CODE_NAME FROM EOM_UCL uclfr WHERE LANG_UID = :frCode AND UC_UID = uc.UC_UID) " +
        "FROM   EOM_UC uc " +
        "WHERE  UC_UID = :codeNr " +
        "INTO   :codeNr, " +
        "       :extKeyColor, " +
        "       :german, " +
        "       :english, " +
        "       :french ",
        new NVPair("deCode", LanguageCodeType.GermanCode.ID),
        new NVPair("enCode", LanguageCodeType.EnglishCode.ID),
        new NVPair("frCode", LanguageCodeType.FrenchCode.ID), formData);

    return formData;
  }

  @Override
  public CodeFormData prepareCreate(CodeFormData formData) throws ProcessingException {
    return formData;
  }

  @Override
  public CodeFormData store(CodeFormData formData) throws ProcessingException {
    SQL.update(
        "UPDATE EOM_UC SET" +
            "       EXT_KEY = :extKeyColor " +
            "WHERE  UC_UID = :codeNr", formData);

    SQL.update(
        "UPDATE EOM_UCL SET" +
            "       CODE_NAME = :german " +
            " WHERE  UC_UID = :codeNr " +
            " AND LANG_UID = :deCode ",
        new NVPair("deCode", LanguageCodeType.GermanCode.ID), formData);

    if (formData.getEnglish().getValue() == null) {
      formData.getEnglish().setValue("");
    }

    SQL.update(
        "UPDATE EOM_UCL SET" +
            "       CODE_NAME = :english " +
            " WHERE  UC_UID = :codeNr " +
            " AND LANG_UID = :enCode ",
        new NVPair("enCode", LanguageCodeType.EnglishCode.ID), formData);

    if (formData.getFrench().getValue() == null) {
      formData.getFrench().setValue("");
    }

    SQL.update(
        "UPDATE EOM_UCL SET" +
            "       CODE_NAME = :french " +
            " WHERE  UC_UID = :codeNr " +
            " AND LANG_UID = :frCode ",
        new NVPair("frCode", LanguageCodeType.FrenchCode.ID), formData);

    CODES.reloadCodeType(formData.getCodeTypeClass());

    return formData;
  }

  @Override
  public int delete(Collection<Long> codeNrs) throws ProcessingException {
    if (CollectionUtility.isEmpty(codeNrs)) {
      throw new ProcessingException("codeNrs == null");
    }
    int deletedItems = 0;
    for (Long codeNr : codeNrs) {
      deletedItems += SQL.delete("DELETE FROM EOM_UC WHERE UC_UID = :codeNr AND UC_UID NOT IN (SELECT SUBJECT_UID FROM EOM_TASK WHERE SUBJECT_UID = :codeNr UNION SELECT SUBJECT_UID FROM EOM_NOTE WHERE SUBJECT_UID = :codeNr UNION SELECT SUBJECT_UID FROM EOM_LESSON WHERE SUBJECT_UID = :codeNr)", new NVPair("codeNr", codeNr));
    }
    if (deletedItems != codeNrs.size()) {
      return -1;
    }

    return deletedItems;
  }
}
