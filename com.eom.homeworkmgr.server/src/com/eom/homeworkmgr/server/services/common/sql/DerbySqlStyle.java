/**
 *
 */
package com.eom.homeworkmgr.server.services.common.sql;

public class DerbySqlStyle extends org.eclipse.scout.rt.services.common.jdbc.style.DerbySqlStyle {

  private static final long serialVersionUID = 1L;

  @Override
  public String createDateBetween(String attribute, String bindName1, String bindName2) {
    return attribute + " BETWEEN " + adaptBindNameTimeDateOp(bindName1) + " AND " + adaptBindNameTimeDateOp(bindName2) + " ";
  }

  @Override
  public String createDateEQ(String attribute, String bindName) {
    return attribute + "=" + adaptBindName(bindName) + "";
  }

  @Override
  public String createDateLE(String attribute, String bindName) {
    return attribute + "<=" + adaptBindName(bindName) + "";
  }

  @Override
  public String createDateLT(String attribute, String bindName) {
    return attribute + "<" + adaptBindName(bindName) + "";
  }

  @Override
  public String createDateGT(String attribute, String bindName) {
    return attribute + ">" + adaptBindName(bindName) + "";
  }

  @Override
  public String createDateGE(String attribute, String bindName) {
    return attribute + ">=" + adaptBindName(bindName) + "";
  }

}
