/**
 *
 */
package com.eom.homeworkmgr.server.services.common.sql;

import org.eclipse.scout.rt.server.services.common.jdbc.style.ISqlStyle;
import org.eclipse.scout.rt.services.common.jdbc.AbstractDerbySqlService;

/**
 * @author ske
 */
public class DerbySqlService extends AbstractDerbySqlService {

  @Override
  protected Class<? extends ISqlStyle> getConfiguredSqlStyle() {
    return com.eom.homeworkmgr.server.services.common.sql.DerbySqlStyle.class;
  }
}
