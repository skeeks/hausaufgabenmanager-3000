/**
 *
 */
package com.eom.homeworkmgr.server.services;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.holders.NVPair;
import org.eclipse.scout.rt.server.services.common.jdbc.SQL;
import org.eclipse.scout.rt.shared.services.common.code.CodeRow;
import org.eclipse.scout.rt.shared.services.common.code.ICodeRow;
import org.eclipse.scout.service.AbstractService;

import com.eom.homeworkmgr.server.ServerSession;
import com.eom.homeworkmgr.shared.services.ISqlCodeTypeLoaderService;

/**
 * Responsible for loading code types from the database. Is used by all CodeTypes which extends AbstractSqlCodeType
 */
public class SqlCodeTypeLoaderService extends AbstractService implements ISqlCodeTypeLoaderService {

  /**
   * @return all available codes of the code type with the UID codeTypeUid.
   */
  @Override
  public List<? extends ICodeRow<Long>> loadCodes(Long codeTypeUid) throws ProcessingException {
    Object[][] data = SQL.select(""
        + "SELECT "
        + " UC.UC_UID, " // Code UID
        + " UCL.CODE_NAME, " // Code Text
        + " UC.PARENT_KEY, " // parentKey
        + " UC.EXT_KEY " // value
        + "FROM EOM_UC UC "
        + "LEFT JOIN EOM_UCL UCL "
        + "ON UCL.UC_UID = UC.UC_UID "
        + "WHERE UC.CODE_TYPE_UID = :codeTypeUid "
        + "AND UCL.LANG_UID = :langNr ",
        new NVPair("codeTypeUid", codeTypeUid),
        new NVPair("langNr", ServerSession.get().getLanguage()));

    List<ICodeRow<Long>> codeRows = new ArrayList<ICodeRow<Long>>();
    for (Object[] dataRow : data) {
      codeRows.add(new CodeRow<Long>((Long) dataRow[0], (String) dataRow[1], null, null, null, null, null, true, (Long) dataRow[2], true, (String) dataRow[3], null, 0));
    }
    return codeRows;
  }
}
