/**
 *
 */
package com.eom.homeworkmgr.server.services;

import java.util.Locale;

import org.eclipse.scout.commons.NumberUtility;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.holders.LongHolder;
import org.eclipse.scout.commons.holders.NVPair;
import org.eclipse.scout.rt.server.services.common.jdbc.SQL;
import org.eclipse.scout.rt.shared.services.common.code.ICode;
import org.eclipse.scout.service.AbstractService;

import com.eom.homeworkmgr.server.ServerSession;
import com.eom.homeworkmgr.shared.lang.LanguageCodeType;
import com.eom.homeworkmgr.shared.services.IEomService;

/**
 * @author ske
 */
public class EomService extends AbstractService implements IEomService {

  @Override
  public void initDbConnection() throws ProcessingException {
    SQL.select("SELECT 1 FROM EOM_SEQ");
  }

  @Override
  public Locale initLocale() throws ProcessingException {
    LongHolder storedLangUid = new LongHolder(0);
    ICode<Long> deCode = new LanguageCodeType().getCode(LanguageCodeType.GermanCode.ID);
    ICode<Long> frCode = new LanguageCodeType().getCode(LanguageCodeType.FrenchCode.ID);

    SQL.select("SELECT LANGUAGE_UID FROM EOM_SEQ INTO :storedLangNr", new NVPair("storedLangNr", storedLangUid));
    if (NumberUtility.nvl(storedLangUid.getValue(), 0L) != 0L) {
      ServerSession.get().setLanguage(storedLangUid.getValue());
      Locale locale = new Locale(new LanguageCodeType().getCode(storedLangUid.getValue()).getExtKey());
      return locale;
    }
    else if (ServerSession.get().getLocale().getLanguage().equals(new Locale(deCode.getExtKey()).getLanguage())) {
      // Set language to German if the loaded local from the system is german.
      ServerSession.get().setLanguage(100L);
    }
    else if (ServerSession.get().getLocale().getLanguage().equals(new Locale(frCode.getExtKey()).getLanguage())) {
      // If not, set the locale to french.
      ServerSession.get().setLanguage(LanguageCodeType.FrenchCode.ID);
    }
    else {
      //set the locale to english
      ServerSession.get().setLanguage(101L);
    }
    return ServerSession.get().getLocale();
  }

  @Override
  public void setLanguage(Long languageUid) throws ProcessingException {
    SQL.update("UPDATE EOM_SEQ SET LANGUAGE_UID = :langUid", new NVPair("langUid", languageUid));
  }
}
