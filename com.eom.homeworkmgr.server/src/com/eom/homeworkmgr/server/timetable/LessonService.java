/**
 *
 */
package com.eom.homeworkmgr.server.timetable;

import org.eclipse.scout.commons.CompareUtility;
import org.eclipse.scout.commons.NumberUtility;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.exception.VetoException;
import org.eclipse.scout.commons.holders.LongHolder;
import org.eclipse.scout.commons.holders.NVPair;
import org.eclipse.scout.rt.server.services.common.jdbc.SQL;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.service.AbstractService;

import com.eom.homeworkmgr.shared.timetable.ILessonService;
import com.eom.homeworkmgr.shared.timetable.LessonFormData;
import com.eom.homeworkmgr.shared.timetable.TimetableFieldData;

/**
 * @author sei
 */
public class LessonService extends AbstractService implements ILessonService {

  @Override
  public LessonFormData create(LessonFormData formData) throws ProcessingException {
    long nextPrimaryKey = SQL.getSequenceNextval("EOM_SEQ");
    if (formData.getTeacher().getValue() == null) {
      formData.getTeacher().setValue("");
    }
    checkForLessonCollision(formData);

    SQL.insert("" +
        "INSERT INTO EOM_LESSON (LESSON_NR, WEEKDAY_UID, START_TIME, END_TIME, SUBJECT_UID, TEACHER) " +
        "VALUES (:primaryKey, :weekday, :startTime, :endTime, :subject, :teacher)"
        , formData, new NVPair("primaryKey", nextPrimaryKey));

    return formData;
  }

  @Override
  public LessonFormData load(LessonFormData formData) throws ProcessingException {
    SQL.selectInto("" +
        "SELECT WEEKDAY_UID, " +
        "       START_TIME, " +
        "       END_TIME, " +
        "       SUBJECT_UID,"
        + "     TEACHER " +
        "FROM   EOM_LESSON " +
        "WHERE  LESSON_NR = :lessonNr " +
        "INTO   :weekday, " +
        "       :startTime, " +
        "       :endTime, " +
        "       :subject, "
        + "     :teacher "
        , formData);

    return formData;
  }

  @Override
  public LessonFormData prepareCreate(LessonFormData formData) throws ProcessingException {
    return formData;
  }

  @Override
  public LessonFormData store(LessonFormData formData) throws ProcessingException {
    checkForLessonCollision(formData);
    if (formData.getTeacher().getValue() == null) {
      formData.getTeacher().setValue("");
    }
    SQL.update(
        "UPDATE EOM_LESSON SET " +
            "       WEEKDAY_UID = :weekday, " +
            "       START_TIME = :startTime, " +
            "       END_TIME = :endTime, " +
            "       SUBJECT_UID = :subject,"
            + "     TEACHER = :teacher " +
            "WHERE  LESSON_NR = :lessonNr", formData);

    return formData;
  }

  @Override
  public void checkForLessonCollision(LessonFormData formData) throws ProcessingException {
    LongHolder result = new LongHolder(0L);
    SQL.selectInto(""
        + "SELECT COUNT(1) "
        + "FROM EOM_LESSON "
        + "WHERE LESSON_NR <> :lessonNrX "
        + "AND WEEKDAY_UID = :weekday "
        + "AND ("
        + "   (START_TIME < :startTime AND END_TIME   > :startTime) "
        + "OR (START_TIME < :endTime   AND END_TIME   > :endTime) "
        + "OR (START_TIME > :startTime AND START_TIME < :endTime) "
        + "OR (END_TIME   > :startTime AND END_TIME   < :endTime) "
        + "OR (START_TIME = :startTime AND END_TIME   = :endTime) "
        + ") "
        + "INTO :result ",
        formData,
        new NVPair("result", result), new NVPair("lessonNrX", NumberUtility.nvl(formData.getLessonNr(), 0L)));
    if (!CompareUtility.equals(result.getValue(), 0L)) {
      throw new VetoException(TEXTS.get("LessonCollisionDetected"));
    }
  }

  @Override
  public void delete(Long timetableNr) throws ProcessingException {
    SQL.delete("DELETE FROM EOM_LESSON WHERE LESSON_NR = :lessonNr", new NVPair("lessonNr", timetableNr));
  }

  @Override
  public TimetableFieldData loadTimetable(TimetableFieldData formData) throws ProcessingException {
    SQL.selectInto(""
        + "SELECT "
        + "   LESSON_NR, "
        + "   WEEKDAY_UID, "
        + "   SUBJECT_UID, "
        + "   START_TIME, "
        + "   END_TIME,"
        + "   TEACHER "
        + "FROM EOM_LESSON "
        + "INTO "
        + "   :lessonNr, "
        + "   :weekday, "
        + "   :subject, "
        + "   :startTime, "
        + "   :endTime,"
        + "   :teacher ",
        formData);

    return formData;
  }
}
