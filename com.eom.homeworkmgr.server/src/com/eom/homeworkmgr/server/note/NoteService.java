/**
 *
 */
package com.eom.homeworkmgr.server.note;

import java.util.Collection;

import org.eclipse.scout.commons.annotations.SqlBindingIgnoreValidation;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.holders.NVPair;
import org.eclipse.scout.rt.server.services.common.jdbc.SQL;
import org.eclipse.scout.rt.server.services.common.jdbc.builder.FormDataStatementBuilder;
import org.eclipse.scout.rt.shared.data.model.DataModelConstants;
import org.eclipse.scout.service.AbstractService;

import com.eom.homeworkmgr.shared.note.INoteService;
import com.eom.homeworkmgr.shared.note.NoteFormData;
import com.eom.homeworkmgr.shared.note.NoteSearchFormData;
import com.eom.homeworkmgr.shared.note.NoteTablePageData;

/**
 * @author sei
 */
public class NoteService extends AbstractService implements INoteService {

  @Override
  @SqlBindingIgnoreValidation({"date", "noteNr", "description"})
  public NoteTablePageData getNoteTableData(NoteSearchFormData formData) throws ProcessingException {
    FormDataStatementBuilder categoriesStatementBuilder = new FormDataStatementBuilder(SQL.getSqlStyle());
    categoriesStatementBuilder.setBasicDefinition(NoteSearchFormData.Subject.class, "SUBJECT_UID", DataModelConstants.OPERATOR_EQ);
    categoriesStatementBuilder.setBasicDefinition(NoteSearchFormData.DateTo.class, "NOTE_DATE", DataModelConstants.OPERATOR_DATE_LE);
    categoriesStatementBuilder.setBasicDefinition(NoteSearchFormData.DateFrom.class, "NOTE_DATE", DataModelConstants.OPERATOR_DATE_GE);
    categoriesStatementBuilder.setBasicDefinition(NoteSearchFormData.Description.class, "DESCRIPTION", DataModelConstants.OPERATOR_LIKE);

    NoteTablePageData pageData = new NoteTablePageData();

    SQL.selectInto("" +
        "SELECT NOTE_NR," +
        "       NOTE_DATE," +
        "       SUBJECT_UID, " +
        "       DESCRIPTION " +
        " FROM  EOM_NOTE" +
        " WHERE 1=1" +
        categoriesStatementBuilder.build(formData) +
        " INTO :noteNr, :date, :subject, :description",
        categoriesStatementBuilder.getBindMap(),
        pageData);

    return pageData;
  }

  @Override
  public NoteFormData create(NoteFormData formData) throws ProcessingException {
    long nextPrimaryKey = SQL.getSequenceNextval("EOM_SEQ");
    SQL.insert("" +
        "INSERT INTO EOM_NOTE (NOTE_NR, NOTE_DATE, DESCRIPTION, SUBJECT_UID) " +
        "VALUES (:primaryKey, :date, :description, :subject)"
        , formData, new NVPair("primaryKey", nextPrimaryKey));

    return formData;
  }

  @Override
  public NoteFormData load(NoteFormData formData) throws ProcessingException {
    SQL.selectInto("" +
        "SELECT NOTE_DATE, " +
        "       DESCRIPTION, " +
        "       SUBJECT_UID " +
        "FROM   EOM_NOTE " +
        "WHERE  NOTE_NR = :noteNr " +
        "INTO   :date," +
        "       :description," +
        "       :subject"
        , formData);

    return formData;
  }

  @Override
  public NoteFormData prepareCreate(NoteFormData formData) throws ProcessingException {
    return formData;
  }

  @Override
  public NoteFormData store(NoteFormData formData) throws ProcessingException {
    SQL.update(
        "UPDATE EOM_NOTE SET" +
            "       NOTE_DATE = :date, " +
            "       DESCRIPTION = :description, " +
            "       SUBJECT_UID = :subject " +
            "WHERE  NOTE_NR = :noteNr", formData);

    return formData;
  }

  @Override
  public int delete(Collection<Long> noteNrs) throws ProcessingException {
    int deletedItems = SQL.delete("DELETE FROM EOM_NOTE WHERE NOTE_NR = :noteNrs", new NVPair("noteNrs", noteNrs));
    return deletedItems;
  }
}
