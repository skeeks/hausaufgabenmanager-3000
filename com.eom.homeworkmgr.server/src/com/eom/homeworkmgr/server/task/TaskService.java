/**
 *
 */
package com.eom.homeworkmgr.server.task;

import java.util.Collection;

import org.eclipse.scout.commons.annotations.SqlBindingIgnoreValidation;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.holders.NVPair;
import org.eclipse.scout.rt.server.services.common.jdbc.SQL;
import org.eclipse.scout.rt.server.services.common.jdbc.builder.FormDataStatementBuilder;
import org.eclipse.scout.rt.shared.data.model.DataModelConstants;
import org.eclipse.scout.service.AbstractService;

import com.eom.homeworkmgr.shared.task.ITaskService;
import com.eom.homeworkmgr.shared.task.TaskFormData;
import com.eom.homeworkmgr.shared.task.TaskSearchFormData;
import com.eom.homeworkmgr.shared.task.TaskTablePageData;

/**
 * @author sei
 */
public class TaskService extends AbstractService implements ITaskService {

  @Override
  @SqlBindingIgnoreValidation({"date", "taskNr", "description", "done"})
  public TaskTablePageData getTaskTableData(TaskSearchFormData formData) throws ProcessingException {
    FormDataStatementBuilder categoriesStatementBuilder = new FormDataStatementBuilder(SQL.getSqlStyle());
    categoriesStatementBuilder.setBasicDefinition(TaskSearchFormData.Description.class, "DESCRIPTION", DataModelConstants.OPERATOR_LIKE);
    categoriesStatementBuilder.setBasicDefinition(TaskSearchFormData.Subject.class, "SUBJECT_UID", DataModelConstants.OPERATOR_EQ);
    categoriesStatementBuilder.setBasicDefinition(TaskSearchFormData.DateTo.class, "TASK_DATE", DataModelConstants.OPERATOR_DATE_LE);
    categoriesStatementBuilder.setBasicDefinition(TaskSearchFormData.DateFrom.class, "TASK_DATE", DataModelConstants.OPERATOR_DATE_GE);

    TaskTablePageData pageData = new TaskTablePageData();
    SQL.selectInto(
        "SELECT TASK_NR," +
            "       TASK_DATE," +
            "       SUBJECT_UID, " +
            "       DESCRIPTION, " +
            "       IS_DONE " +
            " FROM  EOM_TASK" +
            " WHERE 1=1 " +
            " AND IS_EXAM = :isExam " +
            categoriesStatementBuilder.build(formData) +
            " INTO :taskNr, :date, :subject, :description, :done",
        categoriesStatementBuilder.getBindMap(),
        pageData,
        new NVPair("isExam", formData.getExamProperty().getValue()));

    return pageData;
  }

  @Override
  public TaskFormData create(TaskFormData formData) throws ProcessingException {
    long nextPrimaryKey = SQL.getSequenceNextval("EOM_SEQ");
    SQL.insert("" +
        "INSERT INTO EOM_TASK (TASK_NR, TASK_DATE, DESCRIPTION, SUBJECT_UID, IS_EXAM, IS_DONE) " +
        "VALUES (:primaryKey, :date, :description, :subject, :exam, :done)"
        , formData, new NVPair("primaryKey", nextPrimaryKey));

    return formData;
  }

  @Override
  public TaskFormData load(TaskFormData formData) throws ProcessingException {
    SQL.selectInto("" +
        "SELECT TASK_DATE, " +
        "       DESCRIPTION, " +
        "       SUBJECT_UID, " +
        "       IS_DONE " +
        "FROM   EOM_TASK " +
        "WHERE  TASK_NR = :taskNr " +
        "INTO   :date," +
        "       :description," +
        "       :subject, " +
        "       :done"
        , formData);

    return formData;
  }

  @Override
  public TaskFormData prepareCreate(TaskFormData formData) throws ProcessingException {
    return formData;
  }

  @Override
  public TaskFormData store(TaskFormData formData) throws ProcessingException {
    SQL.update(
        "UPDATE EOM_TASK SET" +
            "       TASK_DATE = :date, " +
            "       DESCRIPTION = :description, " +
            "       SUBJECT_UID = :subject, " +
            "       IS_DONE = :done " +
            "WHERE  TASK_NR = :taskNr", formData);

    return formData;
  }

  @Override
  public void updateDone(Long taskNr, Boolean done) throws ProcessingException {
    SQL.update("UPDATE EOM_TASK SET IS_DONE = :done WHERE TASK_NR = :taskNr", new NVPair("taskNr", taskNr), new NVPair("done", done));
  }

  @Override
  public int delete(Collection<Long> taskNrs) throws ProcessingException {
    int deletedItems = SQL.delete("DELETE FROM EOM_TASK WHERE TASK_NR = :taskNrs", new NVPair("taskNrs", taskNrs));
    return deletedItems;
  }
}
