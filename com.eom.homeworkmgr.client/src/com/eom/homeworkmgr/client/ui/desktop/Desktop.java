package com.eom.homeworkmgr.client.ui.desktop;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.scout.commons.CollectionUtility;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.logger.IScoutLogger;
import org.eclipse.scout.commons.logger.ScoutLogManager;
import org.eclipse.scout.rt.client.ClientSyncJob;
import org.eclipse.scout.rt.client.ui.action.ActionFinder;
import org.eclipse.scout.rt.client.ui.action.keystroke.AbstractKeyStroke;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.tool.IToolButton;
import org.eclipse.scout.rt.client.ui.desktop.IDesktop;
import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractFormToolButton;
import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractOutlineViewButton;
import org.eclipse.scout.rt.client.ui.desktop.outline.IOutline;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractSearchForm;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;
import org.eclipse.scout.rt.client.ui.form.IForm;
import org.eclipse.scout.rt.client.ui.form.outline.DefaultOutlineTableForm;
import org.eclipse.scout.rt.client.ui.form.outline.DefaultOutlineTreeForm;
import org.eclipse.scout.rt.extension.client.ui.desktop.AbstractExtensibleDesktop;
import org.eclipse.scout.rt.shared.AbstractIcons;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.ui.UserAgentUtility;

import com.eom.homeworkmgr.client.ClientSession;
import com.eom.homeworkmgr.client.info.HomeworkManagerInfoForm;
import com.eom.homeworkmgr.client.language.SwitchLanguageForm;
import com.eom.homeworkmgr.client.ui.desktop.outlines.StandardOutline;
import com.eom.homeworkmgr.shared.Icons;

// TODO add functionality to switch the languages.
public class Desktop extends AbstractExtensibleDesktop implements IDesktop {
  private static IScoutLogger logger = ScoutLogManager.getLogger(Desktop.class);

  public Desktop() {
  }

  @Override
  protected List<Class<? extends IOutline>> getConfiguredOutlines() {
    List<Class<? extends IOutline>> outlines = new ArrayList<Class<? extends IOutline>>();
    outlines.add(StandardOutline.class);
    return outlines;
  }

  public SearchToolButton getSearchToolButton() {
    return getToolButton(SearchToolButton.class);
  }

  @Override
  protected void execPageSearchFormChanged(IForm oldForm, IForm newForm) throws ProcessingException {
    if (oldForm != null) {
      removeForm(oldForm);
    }
    if (getSearchToolButton() != null) {
      if (newForm != null && newForm instanceof AbstractSearchForm) {

        getSearchToolButton().setEnabled(true);
        getSearchToolButton().setForm((AbstractSearchForm) newForm);
      }
      else {
        getSearchToolButton().setEnabled(false);
        getSearchToolButton().setSelected(false);
      }
    }
  }

  @Override
  public <T extends IToolButton> T getToolButton(Class<? extends T> searchType) {
    // ActionFinder performs instance-of checks. Hence the toolbutton replacement mapping is not required
    return new ActionFinder().findAction(getToolButtons(), searchType);
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("ApplicationTitle");
  }

  @Override
  protected void execOpened() throws ProcessingException {
    //If it is a mobile or tablet device, the DesktopExtension in the mobile plugin takes care of starting the correct forms.
    if (!UserAgentUtility.isDesktopDevice()) {
      return;
    }

    // outline tree
    DefaultOutlineTreeForm treeForm = new DefaultOutlineTreeForm();
    treeForm.setIconId(Icons.EclipseScout);
    treeForm.startView();

    //outline table
    DefaultOutlineTableForm tableForm = new DefaultOutlineTableForm();
    tableForm.setIconId(Icons.EclipseScout);
    tableForm.startView();

    IOutline firstOutline = CollectionUtility.firstElement(getAvailableOutlines());
    if (firstOutline != null) {
      setOutline(firstOutline);
    }

  }

  @Order(10.0)
  public class AboutMenu extends AbstractMenu {

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("AboutMenu");
    }

    @Override
    public void execAction() throws ProcessingException {
      HomeworkManagerInfoForm form = new HomeworkManagerInfoForm();
      form.startModify();
    }
  }

  @Order(20.0)
  public class SwitchLanguageMenu extends AbstractMenu {

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("SwitchLanguageMenu");
    }

    @Override
    public void execAction() throws ProcessingException {
      SwitchLanguageForm form = new SwitchLanguageForm();
      form.startModify();
    }
  }

  @Order(30.0)
  public class ExitMenu extends AbstractMenu {

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("ExitMenu");
    }

    @Override
    public void execAction() throws ProcessingException {
      ClientSyncJob.getCurrentSession(ClientSession.class).stopSession();
    }
  }

  @Order(10.0)
  public class RefreshOutlineKeyStroke extends AbstractKeyStroke {

    @Override
    protected String getConfiguredKeyStroke() {
      return "f5";
    }

    @Override
    protected void execAction() throws ProcessingException {
      if (getOutline() != null) {
        IPage page = getOutline().getActivePage();
        if (page != null) {
          page.reloadPage();
        }
      }
    }
  }

  @Order(10.0)
  public class SearchToolButton extends AbstractFormToolButton<AbstractSearchForm> {

    @Override
    protected String getConfiguredIconId() {
      return AbstractIcons.SmartFieldBrowse;
    }

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Search0");
    }

    @Override
    protected String getConfiguredTooltipText() {
      return TEXTS.get("Search0") + " [F3]";
    }

  }

  @Order(20.0)
  public class SearchToolKeyStroke extends AbstractKeyStroke {

    @Override
    protected String getConfiguredKeyStroke() {
      return "f3";
    }

    @Override
    protected void execAction() throws ProcessingException {
      getSearchToolButton().doAction();
    }
  }

  @Order(10.0)
  public class StandardOutlineViewButton extends AbstractOutlineViewButton {

    public StandardOutlineViewButton() {
      super(Desktop.this, StandardOutline.class);
    }
  }
}
