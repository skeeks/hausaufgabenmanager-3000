/**
 *
 */
package com.eom.homeworkmgr.client.ui.desktop.outlines;

import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;
import org.eclipse.scout.rt.extension.client.ui.desktop.outline.AbstractExtensibleOutline;
import org.eclipse.scout.rt.shared.TEXTS;

import com.eom.homeworkmgr.client.note.NoteTablePage;
import com.eom.homeworkmgr.client.subject.SubjectTablePage;
import com.eom.homeworkmgr.client.task.ExamTablePage;
import com.eom.homeworkmgr.client.task.TaskTablePage;
import com.eom.homeworkmgr.client.timetable.TimetablePage;

/**
 * @author sei
 */
public class StandardOutline extends AbstractExtensibleOutline {

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("StandardOutline");
  }

  @Override
  protected void execCreateChildPages(List<IPage> pageList) throws ProcessingException {
    TimetablePage timetablePage = new TimetablePage();
    pageList.add(timetablePage);
    NoteTablePage noteTablePage = new NoteTablePage();
    pageList.add(noteTablePage);
    TaskTablePage taskTablePage = new TaskTablePage();
    pageList.add(taskTablePage);
    ExamTablePage examTablePage = new ExamTablePage();
    pageList.add(examTablePage);
    SubjectTablePage subjectTablePage = new SubjectTablePage();
    pageList.add(subjectTablePage);
  }
}
