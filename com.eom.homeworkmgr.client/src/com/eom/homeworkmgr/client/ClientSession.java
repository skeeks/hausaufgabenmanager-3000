package com.eom.homeworkmgr.client;

import java.util.Locale;

import javax.swing.JOptionPane;

import org.eclipse.scout.commons.LocaleThreadLocal;
import org.eclipse.scout.commons.UriUtility;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.logger.IScoutLogger;
import org.eclipse.scout.commons.logger.ScoutLogManager;
import org.eclipse.scout.rt.client.AbstractClientSession;
import org.eclipse.scout.rt.client.ClientJob;
import org.eclipse.scout.rt.client.servicetunnel.http.ClientHttpServiceTunnel;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.CODES;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.ui.desktop.Desktop;
import com.eom.homeworkmgr.shared.services.IEomService;

public class ClientSession extends AbstractClientSession {
  private static IScoutLogger logger = ScoutLogManager.getLogger(ClientSession.class);

  public ClientSession() {
    super(true);
  }

  /**
   * @return session in current ThreadContext
   */
  public static ClientSession get() {
    return ClientJob.getCurrentSession(ClientSession.class);
  }

  @Override
  public void execLoadSession() throws ProcessingException {
	logger.info("Initializing Homeworkmanager3000 ....");
    ClientSession.get().goOffline();

    setServiceTunnel(new ClientHttpServiceTunnel(this, UriUtility.toUrl(getBundle().getBundleContext().getProperty("server.url"))));

    // Initialize DB connection and language.
    try {
      IEomService eomService = SERVICES.getService(IEomService.class);
      Locale locale = eomService.initLocale();
      setLocale(locale);
      LocaleThreadLocal.set(locale);
      logger.info("Locale initalized");
    }
    catch (ProcessingException e) {
      JOptionPane.showMessageDialog(null, TEXTS.get("StartupErrorText"), TEXTS.get("StartupErrorTitle"), JOptionPane.ERROR_MESSAGE);
      System.exit(0);
      return;
    }
    setDesktop(new Desktop());

    //pre-load all known code types
    CODES.getAllCodeTypes(com.eom.homeworkmgr.shared.Activator.PLUGIN_ID);
    logger.info("Homeworkmanager3000 initialized");
  }

  @Override
  public void execStoreSession() throws ProcessingException {
  }

  public long getLanguage() {
    return getSharedContextVariable("language", long.class);
  }

}
