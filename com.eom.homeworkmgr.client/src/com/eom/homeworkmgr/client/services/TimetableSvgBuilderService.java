/**
 *
 */
package com.eom.homeworkmgr.client.services;

import java.awt.Color;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.eclipse.scout.commons.HTMLUtility;
import org.eclipse.scout.commons.StringUtility;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.logger.IScoutLogger;
import org.eclipse.scout.commons.logger.ScoutLogManager;
import org.eclipse.scout.rt.shared.services.common.code.CODES;
import org.eclipse.scout.rt.shared.services.common.code.ICode;
import org.eclipse.scout.service.AbstractService;

import com.eom.homeworkmgr.client.timetable.TimetablePageForm;
import com.eom.homeworkmgr.shared.subject.SubjectCodeType;
import com.eom.homeworkmgr.shared.timetable.TimetableFieldData;
import com.eom.homeworkmgr.shared.weekday.WeekdayCodeType;

/**
 * @author ske
 */
public class TimetableSvgBuilderService extends AbstractService implements ITimetableSvgBuilderService {

  private static IScoutLogger logger = ScoutLogManager.getLogger(TimetableSvgBuilderService.class);

  private static HashMap<Long, Integer> WEEKDAY_MAPPING = new HashMap<Long, Integer>(7);

  private HashMap<Long, ICode<Long>> m_codeCache = null;
  private TimetableFieldData m_formData;

  private static final int FONT_SIZE_BIG = 18;
  private static final int FONT_SIZE_MEDIUM = 16;
  private static final int FONT_SIZE_SMALL = 14;

  private static final int COLUMN_LABEL_HEIGHT = 40;

  private static final int COLUMN_WIDTH = 160;
  private static final int COLUMN_HEIGHT = 672; // 42 Pixel per Hour * 16 hours = 672

  private static final int COLUMN_LABEL_OFFSET_TOP = 15;
  private static final int COLUMN_OFFSET_LEFT = 62;
  private static final String COLUMN_COLOR = "#FFFFFF";

  private static final int PIXEL_PER_HOUR = 42; // 16 Hours from 06 - 22

  private static final int TIME_LABEL_OFFSET_LEFT = 10;
  private static final float TIME_LABEL_OFFSET_TOP = 60.5f;

  private static final int LESSON_PADDING_X = 10;
  private static final int TEXT_MARGIN = 4;

  private static final String STROKE_COLOR = "#000000";
  private static final int STROKE_WIDTH = 1;

  // Fill WeekdayMapping
  {
    int x = 0;
    WEEKDAY_MAPPING.put(WeekdayCodeType.MondayCode.ID, 0);
    WEEKDAY_MAPPING.put(WeekdayCodeType.TuesdayCode.ID, x += COLUMN_WIDTH);
    WEEKDAY_MAPPING.put(WeekdayCodeType.WednesdayCode.ID, x += COLUMN_WIDTH);
    WEEKDAY_MAPPING.put(WeekdayCodeType.ThursdayCode.ID, x += COLUMN_WIDTH);
    WEEKDAY_MAPPING.put(WeekdayCodeType.FridayCode.ID, x += COLUMN_WIDTH);
    WEEKDAY_MAPPING.put(WeekdayCodeType.SaturdayCode.ID, x += COLUMN_WIDTH);
    WEEKDAY_MAPPING.put(WeekdayCodeType.SundayCode.ID, x += COLUMN_WIDTH);
  }

  @Override
  public String buildSvgDocument(TimetableFieldData formData) throws ProcessingException {
    m_formData = formData;
    m_codeCache = new HashMap<Long, ICode<Long>>(7);
    StringBuilder builder = new StringBuilder();
    builder.append(createSvgDocumentHead());

    builder.append(getTimeLabelColumn()).toString();
    for (ICode<Long> code : CODES.getCodeType(WeekdayCodeType.class).getCodes()) {
      builder.append(getWeekdayColumn(code).toString());
    }
    builder.append(createSvgDocumentFoot().toString());
    logger.error(builder.toString());
    m_formData = null;
    m_codeCache = null;
    return builder.toString();
  }

  private StringBuilder createSvgDocumentHead() {
    StringBuilder builder = new StringBuilder();

    builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
    builder.append("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"> ");
    builder.append("<svg x='0' y='0' width='1200' height='724' viewBox='0 0 1200 724' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' contentScriptType='text/ecmascript' zoomAndPan='disable'>");
    builder.append("<g id='timetable' style='stroke-width:0;font-family:Calibri;stroke:" + STROKE_COLOR + ";font-size:" + FONT_SIZE_MEDIUM + "' fill='black'>");
    return builder;
  }

  private StringBuilder getTimeLabelColumn() {
    StringBuilder builder = new StringBuilder();
    builder.append("<g transform='translate(" + TIME_LABEL_OFFSET_LEFT + "," + TIME_LABEL_OFFSET_TOP + ")'>");
    int labelCount = 0;
    for (int hour = 6; hour <= 22; hour++) {
      String hourStr = StringUtility.lpad(hour + "", "0", 2);
      float additional = ((hour == 6) ? (FONT_SIZE_MEDIUM / 2.7f) : 0f) - ((hour == 22) ? (FONT_SIZE_MEDIUM / 2.7f) : 0f);
      float labelY = (labelCount * PIXEL_PER_HOUR) - FONT_SIZE_MEDIUM + additional;
      builder.append("<text x='6' y='" + labelY + "' style='font-size:" + FONT_SIZE_MEDIUM + ";'>" + hourStr + ":00</text>");
      labelCount++;
    }
    builder.append("</g>");
    return builder;
  }

  private StringBuilder getWeekdayColumn(ICode<Long> weekdayCode) {
    StringBuilder builder = new StringBuilder();

    int columnX = getColumnX(weekdayCode);

    builder.append("<g>");
    builder.append("<text x='" + (columnX + 10) + "' y='" + (COLUMN_LABEL_HEIGHT - 8) + "' style='font-size:" + FONT_SIZE_BIG + ";'>");
    builder.append(weekdayCode.getText());
    builder.append("</text>");
    builder.append("<a xlink:href='" + TimetablePageForm.LESSON_CREATE_URL_PREFIX + weekdayCode.getId() + "' >");

    int currentY = 0;
    for (int h = 6; h < 22; h++) {
      builder.append("<rect y='" + (COLUMN_LABEL_HEIGHT + currentY) + "' x='" + columnX + "' height='" + (PIXEL_PER_HOUR) + "' width='" + COLUMN_WIDTH + "' style='fill:" + COLUMN_COLOR + ";stroke-width:" + STROKE_WIDTH + ";' />");
      currentY += PIXEL_PER_HOUR;
    }
    builder.append("</a>");
    builder.append("<g transform='translate(" + columnX + "," + COLUMN_LABEL_HEIGHT + ")'>");
    builder.append(getWeekdayLessons(weekdayCode));
    builder.append("</g></g>");
    return builder;
  }

  /**
   * @param x
   *          The x coordinate of the timetable column
   * @param y
   *          the y coordinate of the top of the timetable column
   * @param width
   *          the width of the timetable column
   * @param height
   *          the height of the timetable column.
   * @return
   */
  private StringBuilder getWeekdayLessons(ICode<Long> weekdayCode) {
    StringBuilder builder = new StringBuilder();
    if (m_formData == null) {
      return builder;
    }
    for (int row = 0; row < m_formData.getRowCount(); row++) {
      if (weekdayCode.getId().equals(m_formData.getWeekday(row))) {
        // get the hours.   3600000 = 1000ms * 60s * 60m.
        double lessonY = ((m_formData.getStartTime(row).getTime() / 3600000.0) * PIXEL_PER_HOUR) - (5.0 * PIXEL_PER_HOUR);
        double lessonHeight = ((m_formData.getEndTime(row).getTime() / 3600000.0) * PIXEL_PER_HOUR) - (5.0 * PIXEL_PER_HOUR);
        String subjectColor = CODES.getCodeType(SubjectCodeType.class).getCode(m_formData.getSubject(row)).getExtKey();
        Color color = new Color(
            Integer.valueOf(subjectColor.substring(1, 3), 16),
            Integer.valueOf(subjectColor.substring(3, 5), 16),
            Integer.valueOf(subjectColor.substring(5, 7), 16));
        double brightness = (0.2126 * color.getRed() + 0.7152 * color.getGreen() + 0.0722 * color.getBlue());
        String textColor = (brightness < 127) ? "#FFFFFF" : "#000000";
        lessonHeight = lessonHeight - lessonY; // subtract the x coordinate from the lessonHeight variable so it is the height it must be displayed.
        builder.append("<a xlink:href='" + TimetablePageForm.LESSON_MODIFY_URL_PREFIX + m_formData.getLessonNr(row) + "' >");
        builder.append("<rect x='0' y='" + lessonY + "' height='" + lessonHeight + "' width='" + COLUMN_WIDTH + "' fill='" + ((StringUtility.isNullOrEmpty(subjectColor)) ? "#F1F1F1" : subjectColor) + "' style='stroke-width:" + STROKE_WIDTH + ";' />");
        if (lessonHeight > FONT_SIZE_BIG + (2 * TEXT_MARGIN)) { // same code on line 156
          // --------- Create labels and calculate the height of the labels.
          StringBuilder lessonLabels = new StringBuilder();
          int labelsHeight = 2 * TEXT_MARGIN; // labels height is default to the text margin bottom + top
          // if lessons is higher than the font size used for subject and also the padding (bottom + top).
          if (lessonHeight > FONT_SIZE_BIG + labelsHeight) { // same code on line 151
            String subjectName = HTMLUtility.encodeText(getSubjectCode(m_formData.getSubject(row)).getText());
            labelsHeight += FONT_SIZE_BIG;
            lessonLabels.append("" +
                "<tspan x='" + LESSON_PADDING_X + "' style='font-weight:bold;font-size:" + FONT_SIZE_BIG + ";'>" +
                subjectName +
                "</tspan>");
          }
          // time text
          if (lessonHeight > FONT_SIZE_MEDIUM + TEXT_MARGIN + labelsHeight) {
            String hours = formatAsTime(m_formData.getStartTime(row).getTime());
            String minutes = formatAsTime(m_formData.getEndTime(row).getTime());
            labelsHeight += TEXT_MARGIN + FONT_SIZE_MEDIUM;
            lessonLabels.append("" +
                "<tspan x='" + LESSON_PADDING_X + "' dy='" + (FONT_SIZE_BIG + TEXT_MARGIN) + "' style='font-size:" + FONT_SIZE_MEDIUM + ";'>" + hours + " - " + minutes + "</tspan>");
          }
          // teacher text
          if (lessonHeight > FONT_SIZE_MEDIUM + TEXT_MARGIN + labelsHeight && !StringUtility.isNullOrEmpty(m_formData.getTeacher(row))) {
            labelsHeight += TEXT_MARGIN + FONT_SIZE_MEDIUM;
            lessonLabels.append("" +
                "<tspan x='" + LESSON_PADDING_X + "' dy='" + (FONT_SIZE_MEDIUM + TEXT_MARGIN) + "' style='font-size:" + FONT_SIZE_MEDIUM + ";'>" + HTMLUtility.encodeText(m_formData.getTeacher(row)) + "</tspan>");
          }
          double textY = lessonY + FONT_SIZE_BIG + ((lessonHeight - labelsHeight) / 2.0);
          builder.append("<text x='0' y='" + textY + "' style='fill:" + textColor + ";' shape-rendering='geometricPrecision'>");
          builder.append(lessonLabels);
          builder.append("</text>");
        }

        builder.append("</a>");
      }
    }

    return builder;
  }

  private StringBuilder createSvgDocumentFoot() {
    StringBuilder builder = new StringBuilder();
    builder.append("</g>");
    builder.append("</svg>");
    return builder;
  }

  /**
   * Code caching mechanism
   *
   * @param codeUid
   * @return The corresponding SubjectCode
   */
  private ICode<Long> getSubjectCode(Long codeUid) {
    if (!m_codeCache.containsKey(codeUid)) {
      m_codeCache.put(codeUid, CODES.getCodeType(SubjectCodeType.class).getCode(codeUid));
    }
    return m_codeCache.get(codeUid);
  }

  private int getColumnX(ICode<Long> weekdayCode) {
    return WEEKDAY_MAPPING.get(weekdayCode.getId()).intValue() + COLUMN_OFFSET_LEFT;
  }

  /**
   * Format a timestamp in this format:
   * HH = Hour [0-24]
   * MM = Minutes [0-59]
   * HH:MM
   *
   * @param time
   * @return
   */
  private String formatAsTime(long time) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date(time));
    return StringUtility.lpad(cal.get(Calendar.HOUR_OF_DAY) + "", "0", 2) + ":" + StringUtility.lpad(cal.get(Calendar.MINUTE) + "", "0", 2);
  }
}
