/**
 *
 */
package com.eom.homeworkmgr.client.services;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.service.IService;

import com.eom.homeworkmgr.shared.timetable.TimetableFieldData;

/**
 * @author ske
 */
public interface ITimetableSvgBuilderService extends IService {

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  String buildSvgDocument(TimetableFieldData formData) throws ProcessingException;
}
