/**
 *
 */
package com.eom.homeworkmgr.client.note;

import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.note.NoteForm.MainBox.CancelButton;
import com.eom.homeworkmgr.client.note.NoteForm.MainBox.GroupBox;
import com.eom.homeworkmgr.client.note.NoteForm.MainBox.GroupBox.DateField;
import com.eom.homeworkmgr.client.note.NoteForm.MainBox.GroupBox.DescriptionField;
import com.eom.homeworkmgr.client.note.NoteForm.MainBox.GroupBox.SubjectField;
import com.eom.homeworkmgr.client.note.NoteForm.MainBox.OkButton;
import com.eom.homeworkmgr.shared.note.INoteService;
import com.eom.homeworkmgr.shared.note.NoteFormData;
import com.eom.homeworkmgr.shared.subject.SubjectCodeType;

/**
 * @author sei
 */
@FormData(value = NoteFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class NoteForm extends AbstractForm {

  private Long m_noteNr;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public NoteForm() throws ProcessingException {
    super();
  }

  /**
   * @return the NoteNr
   */
  @FormData
  public Long getNoteNr() {
    return m_noteNr;
  }

  /**
   * @param noteNr
   *          the NoteNr to set
   */
  @FormData
  public void setNoteNr(Long noteNr) {
    m_noteNr = noteNr;
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Note");
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startModify() throws ProcessingException {
    startInternal(new ModifyHandler());
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startNew() throws ProcessingException {
    startInternal(new NewHandler());
  }

  /**
   * @return the CancelButton
   */
  public CancelButton getCancelButton() {
    return getFieldByClass(CancelButton.class);
  }

  /**
   * @return the DateField
   */
  public DateField getDateField() {
    return getFieldByClass(DateField.class);
  }

  /**
   * @return the DescriptionField
   */
  public DescriptionField getDescriptionField() {
    return getFieldByClass(DescriptionField.class);
  }

  /**
   * @return the GroupBox
   */
  public GroupBox getGroupBox() {
    return getFieldByClass(GroupBox.class);
  }

  /**
   * @return the MainBox
   */
  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the OkButton
   */
  public OkButton getOkButton() {
    return getFieldByClass(OkButton.class);
  }

  /**
   * @return the SubjectField
   */
  public SubjectField getSubjectField() {
    return getFieldByClass(SubjectField.class);
  }

  @Order(10.0)
  public class MainBox extends AbstractGroupBox {

    @Order(20.0)
    public class GroupBox extends AbstractGroupBox {

      @Order(10.0)
      public class DateField extends AbstractDateField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Date");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }
      }

      @Order(20.0)
      public class SubjectField extends AbstractSmartField<Long> {

        @Override
        protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
          return SubjectCodeType.class;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Subject0");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }
      }

      @Order(30.0)
      public class DescriptionField extends AbstractStringField {

        @Override
        protected int getConfiguredGridH() {
          return 3;
        }

        @Override
        protected int getConfiguredGridW() {
          return 2;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Description");
        }

        @Override
        protected int getConfiguredLabelPosition() {
          return LABEL_POSITION_TOP;
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }

        @Override
        protected boolean getConfiguredMultilineText() {
          return true;
        }

        @Override
        protected boolean getConfiguredWrapText() {
          return true;
        }
      }
    }

    @Order(40.0)
    public class OkButton extends AbstractOkButton {
    }

    @Order(50.0)
    public class CancelButton extends AbstractCancelButton {
    }
  }

  public class ModifyHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      INoteService service = SERVICES.getService(INoteService.class);
      NoteFormData formData = new NoteFormData();
      exportFormData(formData);
      formData = service.load(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      INoteService service = SERVICES.getService(INoteService.class);
      NoteFormData formData = new NoteFormData();
      exportFormData(formData);
      formData = service.store(formData);

    }
  }

  public class NewHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      INoteService service = SERVICES.getService(INoteService.class);
      NoteFormData formData = new NoteFormData();
      exportFormData(formData);
      formData = service.prepareCreate(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      INoteService service = SERVICES.getService(INoteService.class);
      NoteFormData formData = new NoteFormData();
      exportFormData(formData);
      formData = service.create(formData);

    }
  }
}
