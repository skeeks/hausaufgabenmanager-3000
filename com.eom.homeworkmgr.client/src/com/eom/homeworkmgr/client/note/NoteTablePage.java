/**
 *
 */
package com.eom.homeworkmgr.client.note;

import java.util.Set;

import org.eclipse.scout.commons.CollectionUtility;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.annotations.PageData;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.action.menu.ActivityMapMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TreeMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.ValueFieldMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractDateColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractLongColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractSmartColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.ISearchForm;
import org.eclipse.scout.rt.client.ui.messagebox.MessageBox;
import org.eclipse.scout.rt.extension.client.ui.action.menu.AbstractExtensibleMenu;
import org.eclipse.scout.rt.extension.client.ui.basic.table.AbstractExtensibleTable;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.note.NoteTablePage.Table;
import com.eom.homeworkmgr.shared.note.INoteService;
import com.eom.homeworkmgr.shared.note.NoteSearchFormData;
import com.eom.homeworkmgr.shared.note.NoteTablePageData;
import com.eom.homeworkmgr.shared.subject.SubjectCodeType;

/**
 * @author sei
 */
@PageData(NoteTablePageData.class)
// TODO add delete menu & functionality to NoteTablePage
public class NoteTablePage extends AbstractPageWithTable<Table> {

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Note");
  }

  @Override
  protected void execLoadData(SearchFilter filter) throws ProcessingException {
    NoteSearchFormData formData = null;
    if (filter.getFormData() == null) {
      formData = new NoteSearchFormData();
    }
    else {
      formData = (NoteSearchFormData) filter.getFormData();
    }
    importPageData(SERVICES.getService(INoteService.class).getNoteTableData(formData));
  }

  @Order(10.0)
  public class Table extends AbstractExtensibleTable {

    /**
     * @return the SubjectColumn
     */
    public SubjectColumn getSubjectColumn() {
      return getColumnSet().getColumnByClass(SubjectColumn.class);
    }

    /**
     * @return the DescriptionColumn
     */
    public DescriptionColumn getDescriptionColumn() {
      return getColumnSet().getColumnByClass(DescriptionColumn.class);
    }

    @Override
    protected void execRowAction(ITableRow row) throws ProcessingException {
      super.execRowAction(row);
      getMenu(EditNoteMenu.class).doAction();
    }

    /**
     * @return the DateColumn
     */
    public DateColumn getDateColumn() {
      return getColumnSet().getColumnByClass(DateColumn.class);
    }

    /**
     * @return the NoteNrColumn
     */
    public NoteNrColumn getNoteNrColumn() {
      return getColumnSet().getColumnByClass(NoteNrColumn.class);
    }

    @Order(10.0)
    public class NoteNrColumn extends AbstractLongColumn {

      @Override
      protected boolean getConfiguredDisplayable() {
        return false;
      }

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("NoteNr");
      }

      @Override
      protected int getConfiguredWidth() {
        return 200;
      }
    }

    @Order(20.0)
    public class DateColumn extends AbstractDateColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Date");
      }

      @Override
      protected int getConfiguredWidth() {
        return 125;
      }
    }

    @Order(30.0)
    public class SubjectColumn extends AbstractSmartColumn<Long> {

      @Override
      protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
        return SubjectCodeType.class;
      }

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Subject0");
      }

      @Override
      protected int getConfiguredWidth() {
        return 200;
      }
    }

    @Order(40.0)
    public class DescriptionColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Description");
      }

      @Override
      protected int getConfiguredWidth() {
        return 200;
      }

    }

    @Order(10.0)
    public class NewNoteMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TableMenuType.EmptySpace);
      }

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("NewNote");
      }

      @Override
      protected void execAction() throws ProcessingException {
        NoteForm form = new NoteForm();
        form.startNew();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(20.0)
    public class EditNoteMenu extends AbstractExtensibleMenu {

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("EditNote");
      }

      @Override
      protected void execAction() throws ProcessingException {
        NoteForm form = new NoteForm();
        form.setNoteNr(getNoteNrColumn().getSelectedValue());
        form.startModify();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(30.0)
    public class DeleteMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TableMenuType.MultiSelection, TableMenuType.SingleSelection, ActivityMapMenuType.Activity, TreeMenuType.MultiSelection, TreeMenuType.SingleSelection, ValueFieldMenuType.NotNull);
      }

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("Delete");
      }

      @Override
      protected void execAction() throws ProcessingException {
        if (MessageBox.showYesNoMessage("Verification", null, TEXTS.get("ShureToDelete")) == MessageBox.NO_OPTION) {
          return;
        }
        int deletedItems = SERVICES.getService(INoteService.class).delete(CollectionUtility.arrayList(getNoteNrColumn().getSelectedValues()));
        if (deletedItems > 0) {
          reloadPage();
        }
      }
    }

  }

  @Override
  protected boolean getConfiguredLeaf() {
    return true;
  }

  @Override
  protected Class<? extends ISearchForm> getConfiguredSearchForm() {
    return NoteSearchForm.class;
  }
}
