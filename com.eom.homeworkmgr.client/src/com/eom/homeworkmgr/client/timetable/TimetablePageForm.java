/**
 *
 */
package com.eom.homeworkmgr.client.timetable;

import java.io.ByteArrayInputStream;

import javax.swing.SwingUtilities;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ClientAsyncJob;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.service.SERVICES;
import org.eclipse.scout.svg.client.SVGUtility;
import org.eclipse.scout.svg.client.svgfield.AbstractSvgField;
import org.eclipse.scout.svg.client.svgfield.SvgFieldEvent;
import org.w3c.dom.svg.SVGDocument;

import com.eom.homeworkmgr.client.ClientSession;
import com.eom.homeworkmgr.client.services.ITimetableSvgBuilderService;
import com.eom.homeworkmgr.client.timetable.TimetablePageForm.MainBox.TimetableSvgField;
import com.eom.homeworkmgr.shared.timetable.ILessonService;
import com.eom.homeworkmgr.shared.timetable.TimetableFieldData;

/**
 * @author ske
 */
@FormData(value = TimetableFieldData.class, sdkCommand = FormData.SdkCommand.IGNORE)
public class TimetablePageForm extends AbstractForm {

  public static final String LESSON_CREATE_URL_PREFIX = "http://local/lesson/create/";
  public static final String LESSON_MODIFY_URL_PREFIX = "http://local/lesson/modify/";

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public TimetablePageForm() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Timetable");
  }

  public TimetableSvgField getTimetableSvgField() {
    return getFieldByClass(TimetableSvgField.class);
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startView() throws ProcessingException {
    startInternal(new TimetablePageForm.ViewHandler());
  }

  @Order(10.0)
  public class MainBox extends AbstractGroupBox {

    @Override
    protected boolean getConfiguredGridUseUiHeight() {
      return false;
    }

    @Override
    protected int getConfiguredGridW() {
      return 1;
    }

    @Override
    protected double getConfiguredGridWeightX() {
      return 1.0;
    }

    @Override
    protected double getConfiguredGridWeightY() {
      return 1.0;
    }

    @Override
    protected boolean getConfiguredLabelVisible() {
      return false;
    }

    @Order(10.0)
    public class TimetableSvgField extends AbstractSvgField {

      @Override
      protected double getConfiguredGridWeightX() {
        return 1.0;
      }

      @Override
      protected double getConfiguredGridWeightY() {
        return 1.0;
      }

      @Override
      protected boolean getConfiguredLabelVisible() {
        return false;
      }

      @Override
      protected void execHyperlink(SvgFieldEvent e) throws ProcessingException {
        String url = e.getURL().toString();
        if (url.startsWith(LESSON_MODIFY_URL_PREFIX)) {
          Long entryNr = Long.parseLong(url.replaceFirst(LESSON_MODIFY_URL_PREFIX, ""));
          LessonForm form = new LessonForm();
          form.setLessonNr(entryNr);
          form.startModify();
          form.waitFor();
          if (form.isFormStored()) {
            ClientSession.get().getDesktop().getOutline().getActivePage().reloadPage();
          }
        }
        else if (url.startsWith(LESSON_CREATE_URL_PREFIX)) {
          LessonForm form = new LessonForm();
          form.getWeekdayField().setValue(Long.parseLong(url.replaceFirst(LESSON_CREATE_URL_PREFIX, "")));
          form.startNew();
          form.waitFor();
          if (form.isFormStored()) {
            ClientSession.get().getDesktop().getOutline().getActivePage().reloadPage();
          }
        }
      }
    }

  }

  /**
   * @author ske
   */
  public class ViewHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      final TimetableFieldData formData = SERVICES.getService(ILessonService.class).loadTimetable(new TimetableFieldData());
      new ClientAsyncJob("svgloadjob", ClientSession.get()) {
        @Override
        protected void runVoid(IProgressMonitor monitor) throws Throwable {
          String xml = SERVICES.getService(ITimetableSvgBuilderService.class).buildSvgDocument(formData);
          final SVGDocument document = SVGUtility.readSVGDocument(new ByteArrayInputStream(xml.getBytes()));
          SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              getTimetableSvgField().setSvgDocument(document);
            }
          });
        }

      }.schedule(1);
    }
  }
}
