/**
 *
 */
package com.eom.homeworkmgr.client.timetable;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenuSeparator;
import org.eclipse.scout.rt.client.ui.basic.filechooser.FileChooser;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPage;
import org.eclipse.scout.rt.client.ui.form.FormEvent;
import org.eclipse.scout.rt.client.ui.form.FormListener;
import org.eclipse.scout.rt.client.ui.form.IForm;
import org.eclipse.scout.rt.client.ui.form.PrintDevice;
import org.eclipse.scout.rt.extension.client.ui.action.menu.AbstractExtensibleMenu;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.shell.IShellService;
import org.eclipse.scout.service.SERVICES;

/**
 * @author ske
 */
// TODO TablePage with all the lessons displayed.
public class TimetablePage extends AbstractPage {

  private TimetablePageForm m_form;

  @Override
  protected boolean getConfiguredLeaf() {
    return true;
  }

  @Override
  protected boolean getConfiguredTableVisible() {
    return false;
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Timetable");
  }

  @Override
  protected void execPageActivated() throws ProcessingException {
    m_form = new TimetablePageForm();
    m_form.setDisplayHint(IForm.DISPLAY_HINT_VIEW);
    m_form.setDisplayViewId(IForm.VIEW_ID_CENTER);
    m_form.startView();
  }

  @Override
  protected void execPageDataLoaded() throws ProcessingException {
    if (m_form != null) {
      m_form.doReset();
    }
  }

  @Override
  protected void execPageDeactivated() throws ProcessingException {
    if (m_form != null) {
      m_form.doClose();
      m_form = null;
    }
  }

  public TimetablePageForm getTimetablePageForm() {
    return m_form;
  }

  @Order(10.0)
  public class NewTaskMenu extends AbstractExtensibleMenu {

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("NewLesson");
    }

    @Override
    protected void execAction() throws ProcessingException {
      LessonForm form = new LessonForm();
      form.startNew();
      form.waitFor();
      if (form.isFormStored()) {
        reloadPage();
      }
    }
  }

  @Order(20.0)
  public class SeparatorMenu extends AbstractMenuSeparator {
  }

  @Order(30.0)
  public class ExportTimetableMenu extends AbstractExtensibleMenu {

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("ExportTimetable");
    }

    @Override
    protected void execAction() throws ProcessingException {
      HashMap<String, Object> parameters = new HashMap<String, Object>();
      FileChooser fileChooser = new FileChooser();
      fileChooser.setMultiSelect(false);
      fileChooser.setFolderMode(false);
      fileChooser.setFileExtensions(Arrays.asList("gif"));
      fileChooser.setTypeLoad(false);
      final List<File> files = fileChooser.startChooser();
      if (files != null && files.size() > 0) {
        parameters.put("file", files.get(0));
        m_form.getTimetableSvgField().printField(PrintDevice.File, parameters);
        // add form listener who listens to the Print event. Open the image when events occurs and remove the listener.
        m_form.addFormListener(new FormListener() {
          @Override
          public void formChanged(FormEvent e) throws ProcessingException {
            if (e.getType() == FormEvent.TYPE_PRINT) {
              SERVICES.getService(IShellService.class).shellOpen(files.get(0).getAbsolutePath());
              TimetablePage.this.m_form.removeFormListener(this);
            }
          }
        });

      }
    }
  }
}
