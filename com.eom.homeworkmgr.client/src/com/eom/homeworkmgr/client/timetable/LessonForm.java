/**
 *
 */
package com.eom.homeworkmgr.client.timetable;

import java.util.Date;

import org.eclipse.scout.commons.NumberUtility;
import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.exception.VetoException;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractLinkButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractTimeField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.sequencebox.AbstractSequenceBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.ClientSession;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.CancelButton;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.DeleteButton;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.GroupBox;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.GroupBox.SubjectField;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.GroupBox.TeacherField;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.GroupBox.TimeBox;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.GroupBox.TimeBox.EndTimeField;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.GroupBox.TimeBox.StartTimeField;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.GroupBox.WeekdayField;
import com.eom.homeworkmgr.client.timetable.LessonForm.MainBox.OkButton;
import com.eom.homeworkmgr.shared.subject.SubjectCodeType;
import com.eom.homeworkmgr.shared.timetable.ILessonService;
import com.eom.homeworkmgr.shared.timetable.LessonFormData;
import com.eom.homeworkmgr.shared.weekday.WeekdayCodeType;

/**
 * @author sei
 */
@FormData(value = LessonFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class LessonForm extends AbstractForm {

  private Long m_lessonNr;

  // use 21 and 5 because of timezone
  private static Date TIME_22 = new Date(21 * 60 * 60 * 1000L);
  private static Date TIME_06 = new Date(5 * 60 * 60 * 1000L);

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public LessonForm() throws ProcessingException {
    super();
  }

  /**
   * @return the LessonNr
   */
  @FormData
  public Long getLessonNr() {
    return m_lessonNr;
  }

  @Override
  protected void execInitForm() throws ProcessingException {
    super.execInitForm();
    getDeleteButton().setVisible(NumberUtility.nvl(getLessonNr(), 0L) != 0L);
  }

  /**
   * @param lessonNr
   *          the lessonNr to set
   */
  @FormData
  public void setLessonNr(Long lessonNr) {
    m_lessonNr = lessonNr;
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Lesson");
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startModify() throws ProcessingException {
    startInternal(new ModifyHandler());
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startNew() throws ProcessingException {
    startInternal(new NewHandler());
  }

  /**
   * @return the CancelButton
   */
  public CancelButton getCancelButton() {
    return getFieldByClass(CancelButton.class);
  }

  /**
   * @return the DeleteButton
   */
  public DeleteButton getDeleteButton() {
    return getFieldByClass(DeleteButton.class);
  }

  /**
   * @return the EndTimeField
   */
  public EndTimeField getEndTimeField() {
    return getFieldByClass(EndTimeField.class);
  }

  /**
   * @return the GroupBox
   */
  public GroupBox getGroupBox() {
    return getFieldByClass(GroupBox.class);
  }

  /**
   * @return the MainBox
   */
  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the OkButton
   */
  public OkButton getOkButton() {
    return getFieldByClass(OkButton.class);
  }

  /**
   * @return the StartTimeField
   */
  public StartTimeField getStartTimeField() {
    return getFieldByClass(StartTimeField.class);
  }

  /**
   * @return the SubjectField
   */
  public SubjectField getSubjectField() {
    return getFieldByClass(SubjectField.class);
  }

  /**
   * @return the TeacherField
   */
  public TeacherField getTeacherField() {
    return getFieldByClass(TeacherField.class);
  }

  /**
   * @return the TimeBox
   */
  public TimeBox getTimeBox() {
    return getFieldByClass(TimeBox.class);
  }

  /**
   * @return the WeekdayField
   */
  public WeekdayField getWeekdayField() {
    return getFieldByClass(WeekdayField.class);
  }

  @Order(10.0)
  public class MainBox extends AbstractGroupBox {

    @Order(10.0)
    public class GroupBox extends AbstractGroupBox {

      @Order(10.0)
      public class WeekdayField extends AbstractSmartField<Long> {

        @Override
        protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
          return WeekdayCodeType.class;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Weekday");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }
      }

      @Order(20.0)
      public class SubjectField extends AbstractSmartField<Long> {

        @Override
        protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
          return SubjectCodeType.class;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Subject0");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }
      }

      @Order(30.0)
      public class TeacherField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Teacher");
        }

        @Override
        protected int getConfiguredMaxLength() {
          return 200;
        }
      }

      @Order(60.0)
      public class TimeBox extends AbstractSequenceBox {

        @Order(10.0)
        public class StartTimeField extends AbstractTimeField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Time") + " " + TEXTS.get("from");
          }

          @Override
          protected boolean getConfiguredMandatory() {
            return true;
          }

          @Override
          protected Date execValidateValue(Date rawValue) throws ProcessingException {
            if (rawValue != null && (rawValue.before(TIME_06) || rawValue.after(TIME_22))) {
              throw new VetoException(TEXTS.get("StartTimeOutOfRange"));
            }
            return super.execValidateValue(rawValue);
          }

        }

        @Order(20.0)
        public class EndTimeField extends AbstractTimeField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("to");
          }

          @Override
          protected boolean getConfiguredMandatory() {
            return true;
          }

          @Override
          protected Date execValidateValue(Date rawValue) throws ProcessingException {
            if (rawValue != null && (rawValue.before(TIME_06) || rawValue.after(TIME_22))) {
              throw new VetoException(TEXTS.get("EndTimeOutOfRange"));
            }
            return super.execValidateValue(rawValue);
          }

        }
      }
    }

    @Order(20.0)
    public class DeleteButton extends AbstractLinkButton {

      @Override
      protected String getConfiguredLabel() {
        return TEXTS.get("Delete");
      }

      @Override
      protected void execClickAction() throws ProcessingException {
        SERVICES.getService(ILessonService.class).delete(getLessonNr());
        getForm().doClose();
        ClientSession.get().getDesktop().getOutline().getActivePage().reloadPage();
      }
    }

    @Order(30.0)
    public class OkButton extends AbstractOkButton {
    }

    @Order(40.0)
    public class CancelButton extends AbstractCancelButton {
    }
  }

  public class ModifyHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      ILessonService service = SERVICES.getService(ILessonService.class);
      LessonFormData formData = new LessonFormData();
      exportFormData(formData);
      formData = service.load(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      ILessonService service = SERVICES.getService(ILessonService.class);
      LessonFormData formData = new LessonFormData();
      exportFormData(formData);
      formData = service.store(formData);

    }
  }

  public class NewHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      ILessonService service = SERVICES.getService(ILessonService.class);
      LessonFormData formData = new LessonFormData();
      exportFormData(formData);
      formData = service.prepareCreate(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      ILessonService service = SERVICES.getService(ILessonService.class);
      LessonFormData formData = new LessonFormData();
      exportFormData(formData);
      formData = service.create(formData);

    }
  }
}
