/**
 *
 */
package com.eom.homeworkmgr.client.code;

import java.util.Set;

import org.eclipse.scout.commons.CollectionUtility;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.action.menu.ActivityMapMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TreeMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.ValueFieldMenuType;
import org.eclipse.scout.rt.client.ui.basic.cell.Cell;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractLongColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.messagebox.MessageBox;
import org.eclipse.scout.rt.extension.client.ui.action.menu.AbstractExtensibleMenu;
import org.eclipse.scout.rt.extension.client.ui.basic.table.AbstractExtensibleTable;
import org.eclipse.scout.rt.extension.client.ui.desktop.outline.pages.AbstractExtensiblePageWithTable;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.code.AbstractCodeTablePage.Table;
import com.eom.homeworkmgr.shared.code.ICodePageService;

/**
 * @author sei
 */
// TODO Add delete menu & functionality to table page
public abstract class AbstractCodeTablePage extends AbstractExtensiblePageWithTable<Table> {

  @Override
  protected String getConfiguredTitle() {
    return getConfiguredName();
  }

  public String getConfiguredName() {
    return TEXTS.get("Code");
  }

  @Override
  protected void execLoadData(SearchFilter filter) throws ProcessingException {
    importPageData(SERVICES.getService(ICodePageService.class).getCodeTableData(getCodeTypeUid()));
  }

  public abstract Long getCodeTypeUid();

  public abstract Class<? extends ICodeType> getConfiguredCodeTypeClass();

  public abstract String getConfiguredEditMenuString();

  public abstract String getConfiguredNewMenuString();

  public abstract boolean getConfiguredExtKeyVisible();

  @Order(10.0)
  public class Table extends AbstractExtensibleTable {

    /**
     * @return the ValueColumn
     */
    public ExtKeyColorColumn getExtKeyColorColumn() {
      return getColumnSet().getColumnByClass(ExtKeyColorColumn.class);
    }

    /**
     * @return the GermanColumn
     */
    public GermanColumn getGermanColumn() {
      return getColumnSet().getColumnByClass(GermanColumn.class);
    }

    /**
     * @return the EnglishColumn
     */
    public EnglishColumn getEnglishColumn() {
      return getColumnSet().getColumnByClass(EnglishColumn.class);
    }

    /**
     * @return the FrenchColumn
     */
    public FrenchColumn getFrenchColumn() {
      return getColumnSet().getColumnByClass(FrenchColumn.class);
    }

    /**
     * @return the CodeNrColumn
     */
    public CodeNrColumn getCodeNrColumn() {
      return getColumnSet().getColumnByClass(CodeNrColumn.class);
    }

    @Order(10.0)
    public class CodeNrColumn extends AbstractLongColumn {

      @Override
      protected boolean getConfiguredDisplayable() {
        return false;
      }

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("CodeNr");
      }

      @Override
      protected boolean getConfiguredPrimaryKey() {
        return true;
      }
    }

    @Order(20.0)
    public class ExtKeyColorColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Color");
      }

      @Override
      protected void execDecorateCell(Cell cell, ITableRow row) throws ProcessingException {
        super.execDecorateCell(cell, row);
        cell.setBackgroundColor("" + cell.getValue());
        cell.setText("");
      }
    }

    @Order(30.0)
    public class GermanColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("German");
      }
    }

    @Order(40.0)
    public class EnglishColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("English");
      }
    }

    @Order(50.0)
    public class FrenchColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("French");
      }
    }

    @Order(10.0)
    public class NewCodeMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TableMenuType.EmptySpace);
      }

      @Override
      protected String getConfiguredText() {
        return getConfiguredNewMenuString();
      }

      @Override
      protected void execAction() throws ProcessingException {
        CodeForm form = new CodeForm();
        form.setExtKeyVisible(getConfiguredExtKeyVisible());
        form.setCodeTypeClass(getConfiguredCodeTypeClass());
        form.setFormTitle(getConfiguredName());
        form.setCodeTypeUid(getCodeTypeUid());
        form.startNew();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(20.0)
    public class EditCodeMenu extends AbstractExtensibleMenu {

      @Override
      protected String getConfiguredText() {
        return getConfiguredEditMenuString();
      }

      @Override
      protected void execAction() throws ProcessingException {
        CodeForm form = new CodeForm();
        form.setExtKeyVisible(getConfiguredExtKeyVisible());
        form.setCodeTypeClass(getConfiguredCodeTypeClass());
        form.setFormTitle(getConfiguredName());
        form.setCodeNr(getCodeNrColumn().getSelectedValue());
        form.setCodeTypeUid(getCodeTypeUid());
        form.startModify();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(30.0)
    public class DeleteMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TableMenuType.MultiSelection, TableMenuType.SingleSelection, ActivityMapMenuType.Activity, TreeMenuType.MultiSelection, TreeMenuType.SingleSelection, ValueFieldMenuType.NotNull);
      }

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("Delete");
      }

      @Override
      protected void execAction() throws ProcessingException {
        if (MessageBox.showYesNoMessage(TEXTS.get("Verification"), null, TEXTS.get("ShureToDelete")) == MessageBox.NO_OPTION) {
          return;
        }
        int deletedItems = SERVICES.getService(ICodePageService.class).delete(CollectionUtility.arrayList(getCodeNrColumn().getSelectedValues()));
        if (deletedItems > 0) {
          reloadPage();
        }
        else if (deletedItems == -1) {
          MessageBox.showOkMessage(null, null, TEXTS.get("FailedToDelete"));
        }
      }
    }
  }
}
