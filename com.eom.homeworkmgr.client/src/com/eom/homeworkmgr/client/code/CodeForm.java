/**
 *
 */
package com.eom.homeworkmgr.client.code;

import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.colorpickerfield.AbstractColorField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.code.CodeForm.MainBox.CancelButton;
import com.eom.homeworkmgr.client.code.CodeForm.MainBox.GroupBox;
import com.eom.homeworkmgr.client.code.CodeForm.MainBox.GroupBox.EnglishField;
import com.eom.homeworkmgr.client.code.CodeForm.MainBox.GroupBox.ExtKeyColorField;
import com.eom.homeworkmgr.client.code.CodeForm.MainBox.GroupBox.FrenchField;
import com.eom.homeworkmgr.client.code.CodeForm.MainBox.GroupBox.GermanField;
import com.eom.homeworkmgr.client.code.CodeForm.MainBox.OkButton;
import com.eom.homeworkmgr.shared.code.CodeFormData;
import com.eom.homeworkmgr.shared.code.ICodePageService;

/**
 * @author sei
 */

@FormData(value = CodeFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class CodeForm extends AbstractForm {

  private Long m_codeNr;
  private Long m_codeTypeUid;
  private Class<? extends ICodeType> m_codeTypeClass;
  private boolean m_extKeyVisible;
  private String m_formTitle;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public CodeForm() throws ProcessingException {
    super();
  }

  @Override
  protected void execInitForm() throws ProcessingException {
    super.execInitForm();
    setTitle(getFormTitle());
  }

  /**
   * @return the CodeNr
   */
  @FormData
  public Long getCodeNr() {
    return m_codeNr;
  }

  /**
   * @param codeNr
   *          the CodeNr to set
   */
  @FormData
  public void setCodeNr(Long codeNr) {
    m_codeNr = codeNr;
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startModify() throws ProcessingException {
    startInternal(new ModifyHandler());
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startNew() throws ProcessingException {
    startInternal(new NewHandler());
  }

  /**
   * @return the CancelButton
   */
  public CancelButton getCancelButton() {
    return getFieldByClass(CancelButton.class);
  }

  /**
   * @return the EnglishField
   */
  public EnglishField getEnglishField() {
    return getFieldByClass(EnglishField.class);
  }

  /**
   * @return the ExtKeyColorField
   */
  public ExtKeyColorField getExtKeyColorField() {
    return getFieldByClass(ExtKeyColorField.class);
  }

  /**
   * @return the FrenchField
   */
  public FrenchField getFrenchField() {
    return getFieldByClass(FrenchField.class);
  }

  /**
   * @return the GermanField
   */
  public GermanField getGermanField() {
    return getFieldByClass(GermanField.class);
  }

  /**
   * @return the GroupBox
   */
  public GroupBox getGroupBox() {
    return getFieldByClass(GroupBox.class);
  }

  /**
   * @return the MainBox
   */
  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the OkButton
   */
  public OkButton getOkButton() {
    return getFieldByClass(OkButton.class);
  }

  @Order(10.0)
  public class MainBox extends AbstractGroupBox {

    @Order(10.0)
    public class GroupBox extends AbstractGroupBox {

      @Override
      protected int getConfiguredGridColumnCount() {
        return 1;
      }

      @Override
      protected int getConfiguredGridH() {
        return 2;
      }

      @Order(10.0)
      public class ExtKeyColorField extends AbstractColorField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Color");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }

        @Override
        protected void execInitField() throws ProcessingException {
          super.execInitField();
          setVisible(isExtKeyVisible());
        }
      }

      @Order(20.0)
      public class GermanField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("German");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }
      }

      @Order(30.0)
      public class EnglishField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("English");
        }
      }

      @Order(40.0)
      public class FrenchField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("French");
        }
      }
    }

    @Order(20.0)
    public class OkButton extends AbstractOkButton {
    }

    @Order(30.0)
    public class CancelButton extends AbstractCancelButton {
    }
  }

  public class ModifyHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      ICodePageService service = SERVICES.getService(ICodePageService.class);
      CodeFormData formData = new CodeFormData();
      exportFormData(formData);
      formData = service.load(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      ICodePageService service = SERVICES.getService(ICodePageService.class);
      CodeFormData formData = new CodeFormData();
      exportFormData(formData);
      formData = service.store(formData);

    }
  }

  public class NewHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      ICodePageService service = SERVICES.getService(ICodePageService.class);
      CodeFormData formData = new CodeFormData();
      exportFormData(formData);
      formData = service.prepareCreate(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      ICodePageService service = SERVICES.getService(ICodePageService.class);
      CodeFormData formData = new CodeFormData();
      exportFormData(formData);
      formData = service.create(formData);

    }
  }

  /**
   * @return the CodeTypeClass
   */
  @FormData
  public Class<? extends ICodeType> getCodeTypeClass() {
    return m_codeTypeClass;
  }

  /**
   * @param codeTypeClass
   *          the CodeTypeClass to set
   */
  @FormData
  public void setCodeTypeClass(Class<? extends ICodeType> codeTypeClass) {
    m_codeTypeClass = codeTypeClass;
  }

  /**
   * @return the CodeTypeUid
   */
  @FormData
  public Long getCodeTypeUid() {
    return m_codeTypeUid;
  }

  /**
   * @param codeTypeUid
   *          the CodeTypeUid to set
   */
  @FormData
  public void setCodeTypeUid(Long codeTypeUid) {
    m_codeTypeUid = codeTypeUid;
  }

  /**
   * @return the IsExtKeyDisplayed
   */
  public boolean isExtKeyVisible() {
    return m_extKeyVisible;
  }

  /**
   * @param isExtKeyDisplayed
   *          the IsExtKeyDisplayed to set
   */
  public void setExtKeyVisible(boolean isExtKeyDisplayed) {
    m_extKeyVisible = isExtKeyDisplayed;
  }

  /**
   * @return the FormTitle
   */

  public String getFormTitle() {
    return m_formTitle;
  }

  /**
   * @param formTitle
   *          the FormTitle to set
   */
  public void setFormTitle(String formTitle) {
    m_formTitle = formTitle;
  }
}
