/**
 *
 */
package com.eom.homeworkmgr.client.subject;

import org.eclipse.scout.commons.annotations.PageData;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;

import com.eom.homeworkmgr.client.code.AbstractCodeTablePage;
import com.eom.homeworkmgr.shared.sibject.SubjectTablePageData;
import com.eom.homeworkmgr.shared.subject.SubjectCodeType;

/**
 * @author sei
 */
@PageData(SubjectTablePageData.class)
public class SubjectTablePage extends AbstractCodeTablePage {

  @Override
  protected boolean getConfiguredLeaf() {
    return true;
  }

  @Override
  public boolean getConfiguredExtKeyVisible() {
    return true;
  }

  @Override
  public String getConfiguredName() {
    return TEXTS.get("Subject0");
  }

  @Override
  public Long getCodeTypeUid() {
    return SubjectCodeType.ID;
  }

  @Override
  public Class<? extends ICodeType> getConfiguredCodeTypeClass() {
    return SubjectCodeType.class;
  }

  @Override
  public String getConfiguredEditMenuString() {
    return TEXTS.get("EditSubject");
  }

  @Override
  public String getConfiguredNewMenuString() {
    return TEXTS.get("NewSubject");
  }

}
