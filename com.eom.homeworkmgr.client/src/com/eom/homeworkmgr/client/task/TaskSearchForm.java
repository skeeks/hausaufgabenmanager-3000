/**
 *
 */
package com.eom.homeworkmgr.client.task;

import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractSearchForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractResetButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractSearchButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.listbox.AbstractListBox;
import org.eclipse.scout.rt.client.ui.form.fields.placeholder.AbstractPlaceholderField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.client.ui.form.fields.tabbox.AbstractTabBox;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.ResetButton;
import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.SearchButton;
import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.TabBox;
import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.TabBox.FieldBox;
import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.TabBox.FieldBox.DateFrom;
import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.TabBox.FieldBox.DateTo;
import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.TabBox.FieldBox.DescriptionField;
import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.TabBox.FieldBox.SpacerField;
import com.eom.homeworkmgr.client.task.TaskSearchForm.MainBox.TabBox.FieldBox.SubjectField;
import com.eom.homeworkmgr.shared.subject.SubjectCodeType;
import com.eom.homeworkmgr.shared.task.TaskSearchFormData;

/**
 * @author sei
 */
@FormData(value = TaskSearchFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class TaskSearchForm extends AbstractSearchForm {

  private boolean m_exam;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public TaskSearchForm() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Task");
  }

  @Override
  protected void execResetSearchFilter(SearchFilter searchFilter) throws ProcessingException {
    super.execResetSearchFilter(searchFilter);
    TaskSearchFormData formData = new TaskSearchFormData();
    exportFormData(formData);
    searchFilter.setFormData(formData);
  }

  @Override
  public void startSearch() throws ProcessingException {
    startInternal(new SearchHandler());
  }

  /**
   * @return the DateFrom
   */
  public DateFrom getDateFrom() {
    return getFieldByClass(DateFrom.class);
  }

  /**
   * @return the DateTo
   */
  public DateTo getDateTo() {
    return getFieldByClass(DateTo.class);
  }

  /**
   * @return the DescriptionField
   */
  public DescriptionField getDescriptionField() {
    return getFieldByClass(DescriptionField.class);
  }

  /**
   * @return the FieldBox
   */
  public FieldBox getFieldBox() {
    return getFieldByClass(FieldBox.class);
  }

  /**
   * @return the MainBox
   */
  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the ResetButton
   */
  public ResetButton getResetButton() {
    return getFieldByClass(ResetButton.class);
  }

  /**
   * @return the SearchButton
   */
  public SearchButton getSearchButton() {
    return getFieldByClass(SearchButton.class);
  }

  /**
   * @return the SpacerField
   */
  public SpacerField getSpacerField() {
    return getFieldByClass(SpacerField.class);
  }

  /**
   * @return the SubjectField
   */
  public SubjectField getSubjectField() {
    return getFieldByClass(SubjectField.class);
  }

  /**
   * @return the TabBox
   */
  public TabBox getTabBox() {
    return getFieldByClass(TabBox.class);
  }

  @Order(10.0)
  public class MainBox extends AbstractGroupBox {

    @Order(10.0)
    public class TabBox extends AbstractTabBox {

      @Order(10.0)
      public class FieldBox extends AbstractGroupBox {

        @Override
        protected int getConfiguredGridColumnCount() {
          return 1;
        }

        @Override
        protected int getConfiguredGridH() {
          return 2;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("searchCriteria");
        }

        @Order(10.0)
        public class SubjectField extends AbstractListBox<Long> {

          @Override
          protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
            return SubjectCodeType.class;
          }

          @Override
          protected int getConfiguredGridH() {
            return 3;
          }

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Subject0");
          }
        }

        @Order(20.0)
        public class DateFrom extends AbstractDateField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Date") + " " + TEXTS.get("from");
          }
        }

        @Order(30.0)
        public class DateTo extends AbstractDateField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Date") + " " + TEXTS.get("to");
          }
        }

        @Order(50.0)
        public class DescriptionField extends AbstractStringField {

          @Override
          protected int getConfiguredGridH() {
            return 3;
          }

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Description");
          }

          @Override
          protected boolean getConfiguredMultilineText() {
            return true;
          }

          @Override
          protected boolean getConfiguredWrapText() {
            return true;
          }
        }

        @Order(60.0)
        public class SpacerField extends AbstractPlaceholderField {

          @Override
          protected double getConfiguredGridWeightY() {
            return 1.0;
          }
        }
      }
    }

    @Order(40.0)
    public class ResetButton extends AbstractResetButton {
    }

    @Order(50.0)
    public class SearchButton extends AbstractSearchButton {
    }
  }

  public class SearchHandler extends AbstractFormHandler {
  }

  /**
   * @return the Exam
   */
  @FormData
  public boolean isExam() {
    return m_exam;
  }

  /**
   * @param exam
   *          the Exam to set
   */
  @FormData
  public void setExam(boolean exam) {
    m_exam = exam;
  }
}
