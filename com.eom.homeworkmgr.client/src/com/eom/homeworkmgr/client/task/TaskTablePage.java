/**
 *
 */
package com.eom.homeworkmgr.client.task;

import org.eclipse.scout.commons.annotations.PageData;
import org.eclipse.scout.rt.shared.TEXTS;

import com.eom.homeworkmgr.shared.task.TaskTablePageData;

/**
 * @author sei
 */
@PageData(TaskTablePageData.class)
public class TaskTablePage extends AbstractTaskTablePage {

  public TaskTablePage() {
    super();
    setExam(false);
  }

  @Override
  protected boolean getConfiguredLeaf() {
    return true;
  }

  @Override
  public String getConfiguredName() {
    return TEXTS.get("Task");
  }

}
