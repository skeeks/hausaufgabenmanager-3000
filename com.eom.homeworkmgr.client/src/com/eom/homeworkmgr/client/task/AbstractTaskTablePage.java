/**
 *
 */
package com.eom.homeworkmgr.client.task;

import java.util.Set;

import org.eclipse.scout.commons.CollectionUtility;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.annotations.PageData;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.action.menu.ActivityMapMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TreeMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.ValueFieldMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBooleanColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractDateColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractLongColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractSmartColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.ISearchForm;
import org.eclipse.scout.rt.client.ui.form.fields.IFormField;
import org.eclipse.scout.rt.client.ui.form.fields.booleanfield.AbstractBooleanField;
import org.eclipse.scout.rt.client.ui.messagebox.MessageBox;
import org.eclipse.scout.rt.extension.client.ui.action.menu.AbstractExtensibleMenu;
import org.eclipse.scout.rt.extension.client.ui.basic.table.AbstractExtensibleTable;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.task.AbstractTaskTablePage.Table;
import com.eom.homeworkmgr.shared.subject.SubjectCodeType;
import com.eom.homeworkmgr.shared.task.AbstractTaskTablePageData;
import com.eom.homeworkmgr.shared.task.ITaskService;
import com.eom.homeworkmgr.shared.task.TaskSearchFormData;

/**
 * @author sei
 */
@PageData(AbstractTaskTablePageData.class)
public abstract class AbstractTaskTablePage extends AbstractPageWithTable<Table> {

  private boolean exam;

  /**
   * @return the isExam
   */
  public boolean isExam() {
    return exam;
  }

  /**
   * @param isExam
   *          the isExam to set
   */
  public void setExam(boolean isExam) {
    this.exam = isExam;
  }

  @Override
  public String getConfiguredTitle() {
    return getConfiguredName();
  }

  public abstract String getConfiguredName();

  @Override
  protected void execLoadData(SearchFilter filter) throws ProcessingException {
    TaskSearchFormData formData = null;
    if (filter.getFormData() == null) {
      formData = new TaskSearchFormData();
    }
    else {
      formData = (TaskSearchFormData) filter.getFormData();
    }
    formData.setExam(isExam());
    importPageData(SERVICES.getService(ITaskService.class).getTaskTableData(formData));
  }

  public String getConfiguredNewTaskText() {
    return TEXTS.get("NewTask");
  }

  public String getConfiguredEditTaskText() {
    return TEXTS.get("EditTask");
  }

  @Order(10.0)
  public class Table extends AbstractExtensibleTable {

    @Override
    protected void execRowAction(ITableRow row) throws ProcessingException {
      super.execRowAction(row);
      getMenu(EditTaskMenu.class).doAction();
    }

    /**
     * @return the Subject0Column
     */
    public SubjectColumn getSubjectColumn() {
      return getColumnSet().getColumnByClass(SubjectColumn.class);
    }

    /**
     * @return the DescriptionColumn
     */
    public DescriptionColumn getDescriptionColumn() {
      return getColumnSet().getColumnByClass(DescriptionColumn.class);
    }

    /**
     * @return the DateColumn
     */
    public DateColumn getDateColumn() {
      return getColumnSet().getColumnByClass(DateColumn.class);
    }

    /**
     * @return the TaskNrColumn
     */
    public TaskNrColumn getTaskNrColumn() {
      return getColumnSet().getColumnByClass(TaskNrColumn.class);
    }

    @Order(10.0)
    public class TaskNrColumn extends AbstractLongColumn {

      @Override
      protected boolean getConfiguredDisplayable() {
        return false;
      }

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("TaskNr");
      }

      @Override
      protected int getConfiguredWidth() {
        return 200;
      }
    }

    @Order(20.0)
    public class DateColumn extends AbstractDateColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Date");
      }

      @Override
      protected int getConfiguredWidth() {
        return 125;
      }
    }

    @Order(30.0)
    public class SubjectColumn extends AbstractSmartColumn<Long> {

      @Override
      protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
        return SubjectCodeType.class;
      }

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Subject0");
      }

      @Override
      protected int getConfiguredWidth() {
        return 200;
      }
    }

    @Order(40.0)
    public class DescriptionColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Description");
      }

      @Override
      protected int getConfiguredWidth() {
        return 200;
      }
    }

    @Order(50.0)
    public class DoneColumn extends AbstractBooleanColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Done");
      }

      @Override
      protected int getConfiguredWidth() {
        return 100;
      }

      @Override
      protected boolean getConfiguredEditable() {
        return true;
      }

      @Override
      protected void execCompleteEdit(ITableRow row, IFormField field) throws ProcessingException {
        SERVICES.getService(ITaskService.class).updateDone((Long) row.getCellValue(0), ((AbstractBooleanField) field).getValue());
        reloadPage();
      }
    }

    @Order(10.0)
    public class EditTaskMenu extends AbstractExtensibleMenu {

      @Override
      protected String getConfiguredText() {
        return getConfiguredEditTaskText();
      }

      @Override
      protected void execAction() throws ProcessingException {
        TaskForm form = new TaskForm();
        form.setFormTitle(getConfiguredName());
        form.setTaskNr(getTaskNrColumn().getSelectedValue());
        form.startModify();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(20.0)
    public class NewTaskMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TableMenuType.EmptySpace);
      }

      @Override
      protected String getConfiguredText() {
        return getConfiguredNewTaskText();
      }

      @Override
      protected void execAction() throws ProcessingException {
        TaskForm form = new TaskForm();
        form.setFormTitle(getConfiguredName());
        form.setExam(isExam());
        form.startNew();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(30.0)
    public class DeleteMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TableMenuType.MultiSelection, TableMenuType.SingleSelection, ActivityMapMenuType.Activity, TreeMenuType.MultiSelection, TreeMenuType.SingleSelection, ValueFieldMenuType.NotNull);
      }

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("Delete");
      }

      @Override
      protected void execAction() throws ProcessingException {
        if (MessageBox.showYesNoMessage("Verification", null, TEXTS.get("ShureToDelete")) == MessageBox.NO_OPTION) {
          return;
        }
        int deletedItems = SERVICES.getService(ITaskService.class).delete(CollectionUtility.arrayList(getTaskNrColumn().getSelectedValues()));
        if (deletedItems > 0) {
          reloadPage();
        }
      }
    }
  }

  @Override
  protected Class<? extends ISearchForm> getConfiguredSearchForm() {
    return TaskSearchForm.class;
  }
}
