/**
 *
 */
package com.eom.homeworkmgr.client.task;

import org.eclipse.scout.commons.annotations.PageData;
import org.eclipse.scout.rt.shared.TEXTS;

import com.eom.homeworkmgr.shared.task.ExamTablePageData;

/**
 * @author sei
 */
@PageData(ExamTablePageData.class)
public class ExamTablePage extends AbstractTaskTablePage {

  public ExamTablePage() {
    super();
    setExam(true);
  }

  @Override
  protected boolean getConfiguredLeaf() {
    return true;
  }

  @Override
  public String getConfiguredName() {
    return TEXTS.get("Exam");
  }

  @Override
  public String getConfiguredNewTaskText() {
    return TEXTS.get("NewExam");
  }

  @Override
  public String getConfiguredEditTaskText() {
    return TEXTS.get("EditExam");
  }
}
