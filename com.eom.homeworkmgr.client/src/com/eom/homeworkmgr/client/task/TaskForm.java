/**
 *
 */
package com.eom.homeworkmgr.client.task;

import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.booleanfield.AbstractBooleanField;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.IGroupBoxBodyGrid;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.internal.HorizontalGroupBoxBodyGrid;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.task.TaskForm.MainBox.CancelButton;
import com.eom.homeworkmgr.client.task.TaskForm.MainBox.GroupBox;
import com.eom.homeworkmgr.client.task.TaskForm.MainBox.GroupBox.DescriptionField;
import com.eom.homeworkmgr.client.task.TaskForm.MainBox.GroupBox.TopBox;
import com.eom.homeworkmgr.client.task.TaskForm.MainBox.GroupBox.TopBox.DateField;
import com.eom.homeworkmgr.client.task.TaskForm.MainBox.GroupBox.TopBox.DoneField;
import com.eom.homeworkmgr.client.task.TaskForm.MainBox.GroupBox.TopBox.SubjectField;
import com.eom.homeworkmgr.client.task.TaskForm.MainBox.OkButton;
import com.eom.homeworkmgr.shared.subject.SubjectCodeType;
import com.eom.homeworkmgr.shared.task.ITaskService;
import com.eom.homeworkmgr.shared.task.TaskFormData;

/**
 * @author sei
 */
// TODO Delete Menu functionality for Tasks
@FormData(value = TaskFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class TaskForm extends AbstractForm {

  private long m_taskNr;
  private boolean m_exam;
  private String m_formTitle;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public TaskForm() throws ProcessingException {
    super();
  }

  @Override
  protected void execInitForm() throws ProcessingException {
    super.execInitForm();
    setTitle(getFormTitle());
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Task");
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startModify() throws ProcessingException {
    startInternal(new ModifyHandler());
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startNew() throws ProcessingException {
    startInternal(new NewHandler());
  }

  /**
   * @return the CancelButton
   */
  public CancelButton getCancelButton() {
    return getFieldByClass(CancelButton.class);
  }

  /**
   * @return the DateField
   */
  public DateField getDateField() {
    return getFieldByClass(DateField.class);
  }

  /**
   * @return the DescriptionField
   */
  public DescriptionField getDescriptionField() {
    return getFieldByClass(DescriptionField.class);
  }

  /**
   * @return the DoneField
   */
  public DoneField getDoneField() {
    return getFieldByClass(DoneField.class);
  }

  /**
   * @return the GroupBox
   */
  public GroupBox getGroupBox() {
    return getFieldByClass(GroupBox.class);
  }

  /**
   * @return the MainBox
   */
  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the OkButton
   */
  public OkButton getOkButton() {
    return getFieldByClass(OkButton.class);
  }

  /**
   * @return the SubjectField
   */
  public SubjectField getSubjectField() {
    return getFieldByClass(SubjectField.class);
  }

  /**
   * @return the TopBox
   */
  public TopBox getTopBox() {
    return getFieldByClass(TopBox.class);
  }

  @Order(10.0)
  public class MainBox extends AbstractGroupBox {

    @Order(10.0)
    public class GroupBox extends AbstractGroupBox {
      @Order(10.0)
      public class TopBox extends AbstractGroupBox {
        @Override
        protected Class<? extends IGroupBoxBodyGrid> getConfiguredBodyGrid() {
          return HorizontalGroupBoxBodyGrid.class;
        }

        @Override
        protected int getConfiguredGridColumnCount() {
          return 5;
        }

        @Override
        protected int getConfiguredGridW() {
          return 2;
        }

        @Order(10.0)
        public class DateField extends AbstractDateField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Date");
          }

          @Override
          protected int getConfiguredLabelPosition() {
            return LABEL_POSITION_TOP;
          }

          @Override
          protected boolean getConfiguredMandatory() {
            return true;
          }
        }

        @Order(20.0)
        public class SubjectField extends AbstractSmartField<Long> {

          @Override
          protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
            return SubjectCodeType.class;
          }

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Subject0");
          }

          @Override
          protected int getConfiguredLabelPosition() {
            return LABEL_POSITION_TOP;
          }

          @Override
          protected boolean getConfiguredMandatory() {
            return true;
          }

        }

        @Order(30.0)
        public class DoneField extends AbstractBooleanField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Done");
          }

          @Override
          protected int getConfiguredVerticalAlignment() {
            return 1;
          }
        }
      }

      @Order(20.0)
      public class DescriptionField extends AbstractStringField {

        @Override
        protected int getConfiguredGridH() {
          return 3;
        }

        @Override
        protected int getConfiguredGridW() {
          return 2;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Description");
        }

        @Override
        protected int getConfiguredLabelPosition() {
          return LABEL_POSITION_TOP;
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }

        @Override
        protected boolean getConfiguredMultilineText() {
          return true;
        }

        @Override
        protected boolean getConfiguredWrapText() {
          return true;
        }
      }
    }

    @Order(20.0)
    public class OkButton extends AbstractOkButton {
    }

    @Order(30.0)
    public class CancelButton extends AbstractCancelButton {
    }
  }

  public class ModifyHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      ITaskService service = SERVICES.getService(ITaskService.class);
      TaskFormData formData = new TaskFormData();
      exportFormData(formData);
      formData = service.load(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      ITaskService service = SERVICES.getService(ITaskService.class);
      TaskFormData formData = new TaskFormData();
      exportFormData(formData);
      formData = service.store(formData);

    }
  }

  public class NewHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      ITaskService service = SERVICES.getService(ITaskService.class);
      TaskFormData formData = new TaskFormData();
      exportFormData(formData);
      formData = service.prepareCreate(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      ITaskService service = SERVICES.getService(ITaskService.class);
      TaskFormData formData = new TaskFormData();
      exportFormData(formData);
      formData = service.create(formData);

    }
  }

  /**
   * @return the taskNr
   */
  @FormData
  public long getTaskNr() {
    return m_taskNr;
  }

  /**
   * @param taskNr
   *          the taskNr to set
   */
  @FormData
  public void setTaskNr(long taskNr) {
    m_taskNr = taskNr;
  }

  /**
   * @return the Exam
   */
  @FormData
  public boolean isExam() {
    return m_exam;
  }

  /**
   * @param exam
   *          the Exam to set
   */
  @FormData
  public void setExam(boolean exam) {
    m_exam = exam;
  }

  /**
   * @return the FormTitle
   */

  public String getFormTitle() {
    return m_formTitle;
  }

  /**
   * @param formTitle
   *          the FormTitle to set
   */
  public void setFormTitle(String formTitle) {
    m_formTitle = formTitle;
  }
}
