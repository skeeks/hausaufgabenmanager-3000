/**
 *
 */
package com.eom.homeworkmgr.client.language;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.messagebox.MessageBox;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.service.SERVICES;

import com.eom.homeworkmgr.client.ClientSession;
import com.eom.homeworkmgr.client.language.SwitchLanguageForm.MainBox.CancelButton;
import com.eom.homeworkmgr.client.language.SwitchLanguageForm.MainBox.GroupBox;
import com.eom.homeworkmgr.client.language.SwitchLanguageForm.MainBox.GroupBox.LanguageField;
import com.eom.homeworkmgr.client.language.SwitchLanguageForm.MainBox.OkButton;
import com.eom.homeworkmgr.shared.lang.LanguageCodeType;
import com.eom.homeworkmgr.shared.services.IEomService;

/**
 * @author ske
 */
public class SwitchLanguageForm extends AbstractForm {

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public SwitchLanguageForm() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("SwitchLanguage");
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startModify() throws ProcessingException {
    startInternal(new ModifyHandler());
  }

  /**
   * @return the CancelButton
   */
  public CancelButton getCancelButton() {
    return getFieldByClass(CancelButton.class);
  }

  /**
   * @return the GroupBox
   */
  public GroupBox getGroupBox() {
    return getFieldByClass(GroupBox.class);
  }

  /**
   * @return the LanguageField
   */
  public LanguageField getLanguageField() {
    return getFieldByClass(LanguageField.class);
  }

  /**
   * @return the MainBox
   */
  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the OkButton
   */
  public OkButton getOkButton() {
    return getFieldByClass(OkButton.class);
  }

  @Order(10.0)
  public class MainBox extends AbstractGroupBox {

    @Order(10.0)
    public class GroupBox extends AbstractGroupBox {

      @Order(10.0)
      public class LanguageField extends AbstractSmartField<Long> {

        @Override
        protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
          return LanguageCodeType.class;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Language");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }
      }
    }

    @Order(20.0)
    public class OkButton extends AbstractOkButton {
    }

    @Order(30.0)
    public class CancelButton extends AbstractCancelButton {
    }
  }

  public class ModifyHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      getLanguageField().setValue(ClientSession.get().getLanguage());
    }

    @Override
    protected void execStore() throws ProcessingException {
      SERVICES.getService(IEomService.class).setLanguage(getLanguageField().getValue());
      MessageBox.showOkMessage(TEXTS.get("RestartApplicationTitle"), TEXTS.get("RestartApplicationText"), null);
    }
  }
}
