################################
### Hausaufgabenmanager 3000 ###
##### Homeworkmanager 3000 #####
################################

This project is about making an application which allows you to manage all of your homework. (for only one user per computer)
Tasks:
- list/edit subjects
- list/edit homework specific for the subjects entered

# How to set up a local copy

1. Clone repository: from `git clone https://sixtisam@bitbucket.org/sixtisam/hausaufgabenmanager-3000.git`
3. Update your `config.ini`: `com.eom.homeworkmgr.server.services.common.sql.DerbySqlService#JdbcMappingName=jdbc:derby:<path-to-git-directory>\\com.eom.homeworkmgr.data\\homeworkmgr.db`
4. Update `installdb.bat` in `com.eom.homeworkmgr.database`: `SET DERBY_HOME=<path-to-your-git-directory>\com.eom.homeworkmgr.database\db-derby-10.9.1.0-bin`
5. Execute the `installdb.bat` script in your commandline. Working directory must be `com.eom.homeworkmgr.database`.
6. Synchronize the `homeworkmgr-swing-client-dev.product'.
7. Start the product `homeworkmgr-swing-client-dev.product`