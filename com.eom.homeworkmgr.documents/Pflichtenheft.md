# Pflichtenheft
## Ausgangslage
Mit dem Hausaufgabenmanager3000 soll ein neues Standardprodukt entwickelt werden, welches Schülern und Studenten eine Möglichkeit bietet, ihren Stundenplan zu erfassen und auf Grund von diesem, Aufgaben einzutragen.
### Anstoss für die Beschaffung 
Der Anstoss für die Beschaffung ist die hohe Nachfrage von Schülern und Studenten nach einem Hausaufgabenverwaltungs-Programm.
### Projektorganisation
| Person           | Rolle                                        | 
| -----------------|----------------------------------------------|
| Julian Ackermann | Adminstratives Services Officer              |
| Samuel Keusch    | Senior Software Engineer, Product Manager    |
| Samuel Eiben     | Software Engineer                            |

##	Ziele

* Erfassen des Stundenplans
* Erfassen von Hausaufgaben, verknüpft mit dem Stundenplan
* Erfassen von Fächern
* Erfassen von Notizen, verknüpft mit Fächern

## Anforderung
Siehe Dokument "Technische Spezifikation".

## Anforderung an die Systemplattform
* 1 GigaByte RAM (32 Bit); 2 GigaByte RAM (64 Bit)
* Windows Vista oder neuer
* Java JRE v1.6 oder neuer
* x86- oder x64-Prozessor mit mindestens 1 Gigahertz (GHz)
* mindestens 1024x768px Bildschirmauflösung
* 100 MegaByte freier Festplattenspeicher


## Mengengerüst
Da die Applikation nur lokal läuft ist die Anzahl der bewegten und erfassten Daten unbedeutend. Der Datenbestand ist begrenzt durch den freien Festplattenspeicher des Benutzers.